import {auth, connectDB, validate} from "server/middleware";
import {communityQuestionController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {updateQuestion} from "server/validations/community-question.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res));
  await runMiddleware(req, res, validate(req, res, updateQuestion));

  if (req.method === "POST") {
    await communityQuestionController.updateQuestion(req, res);
  }
});

export default connectDB(handler);