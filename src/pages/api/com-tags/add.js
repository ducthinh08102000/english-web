import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {communityTagController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {createTag} from "server/validations/community-tag.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res));
  await runMiddleware(req, res, validate(req, res, createTag));

  if (req.method === "POST") {
    try {
      await communityTagController.addTag(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);