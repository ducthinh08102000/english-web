import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {commentController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {getComments} from "server/validations/comment.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  if (req.cookies.access_token || req.headers.authorization || req.body.access_token) {
    await runMiddleware(req, res, auth(req, res));
  }
  await runMiddleware(req, res, validate(req, res, getComments));

  if (req.method === "GET") {
    try {
      await commentController.getComments(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);