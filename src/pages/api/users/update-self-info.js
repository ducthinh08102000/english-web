import {auth, connectDB, validate, errorHandler} from "server/middleware";
import {userController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {updateSelfProfile} from "server/validations/user.validation";
import uploadConfig from "server/config/upload.config";
import uploadData from "server/middleware/upload.middleware";

export const config = {
  api: {
    bodyParser: false
  }
};

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res));
  await runMiddleware(req, res, validate(req, res, updateSelfProfile));
  await runMiddleware(req, res, uploadData(req, res, uploadConfig.avatar.exts));

  if (req.method === "POST") {
    try {
      await userController.updateSelfProfile(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);
