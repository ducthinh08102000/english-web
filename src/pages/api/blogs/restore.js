import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {blogController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {restoreBlogs} from "server/validations/blog.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, "MANAGE_ALL_BLOG"));
  await runMiddleware(req, res, validate(req, res, restoreBlogs));

  if (req.method === "POST") {
    try {
      await blogController.restoreBlogs(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);