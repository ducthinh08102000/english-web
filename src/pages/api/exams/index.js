import {auth, connectDB, errorHandler} from "server/middleware";
import {examController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";

const handler = (async (req, res) => {
  await middlewares(req, res);
  if (req.cookies.access_token || req.headers.authorization || req.body.access_token) {
    await runMiddleware(req, res, auth(req, res));
  }
  // await runMiddleware(req, res, validate(req, res, updateSelfProfile));

  if (req.method === "GET") {
    try {
      await examController.getExams(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);
