import connectDB from "server/middleware/mongodb.middleware";
import {Bot} from "server/bot/bot";
import parseTextMessage from "server/bot/parse-text-message";
import parsePostback from "server/bot/parse-postback";
import Message from "server/bot/lib/message";
import {BotUser} from "server/models";
import BotUserHandler from "../../../server/bot/bot-user-handler";

const handler = async (req, res) => {
  Bot.setCredentials(process.env.ACCESS_TOKEN);
  if (req.method === "GET") {
    Bot.verify(req, res);
  } else {
    const received = req.body;
    const entries = received['entry'];
    const receivedObjectType = received['object'];

    if (receivedObjectType === 'page') {
      for (const entry of entries) {
        const pageId = entry['id'];
        if (entry['messaging']) {
          const messaging = entry['messaging'];
          for (const messagingEntry of messaging) {
            const msg = messagingEntry['message'];
            const senderId = messagingEntry['sender']['id'];
            await Bot.send(new Message(senderId).seen());
            await Bot.send(new Message(senderId).typing());
            const user = new BotUserHandler(senderId);
            await user.init();

            if (msg) {
              await parseTextMessage(messagingEntry, user);
            } else if (messagingEntry['postback']) {
              await parsePostback(messagingEntry, user);
            }
          }
        }
      }
    }
    res.json({});
  }
};

export default connectDB(handler);
