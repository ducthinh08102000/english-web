import {auth, connectDB, validate} from "server/middleware";
import {questionController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
// import {deleteQUESTION} from "server/validations/QUESTION.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, "MANAGE_ALL_EXAM_QUESTION", "DELETE_ALL_EXAM_QUESTION"));
  // await runMiddleware(req, res, validate(req, res, deleteQUESTION));

  if (req.method === "POST") {
    await questionController.updateQuestion(req, res);
  }
});

export default connectDB(handler);
