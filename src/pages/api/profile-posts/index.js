import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {profilePostController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {getPosts} from "server/validations/profile-post.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  if (req.cookies.access_token || req.headers.authorization || req.body.access_token) {
    await runMiddleware(req, res, auth(req, res));
  }
  await runMiddleware(req, res, validate(req, res, getPosts));

  if (req.method === "GET") {
    try {
      await profilePostController.getPosts(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);