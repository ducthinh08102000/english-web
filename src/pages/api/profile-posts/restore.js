import {auth, connectDB, errorHandler, validate} from "server/middleware";
import {profilePostController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";
import {restorePost} from "server/validations/profile-post.validation";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, "MANAGE_ALL_BLOG"));
  await runMiddleware(req, res, validate(req, res, restorePost));

  if (req.method === "POST") {
    try {
      await profilePostController.restorePosts(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);