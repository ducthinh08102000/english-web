import {auth, connectDB, errorHandler} from "server/middleware";
import {userActivityController} from "server/controllers";
import middlewares from "server";
import {runMiddleware} from "server/utils/middleware-handler";

const handler = (async (req, res) => {
  await middlewares(req, res);
  await runMiddleware(req, res, auth(req, res, "MANAGE_ALL_USER", "GET_ALL_USER"));

  if (req.method === "GET") {
    try {
      await userActivityController.getUserActivities(req, res);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
});

export default connectDB(handler);
