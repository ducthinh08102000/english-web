import React from "react";
import {Divider} from "antd";
import UserForm from "common/components/manage-components/user-form";
import ManageLayout from "common/layouts/manage-layout";
import RedirectConfig from "../../../../common/configs/RedirectConfig";
import Utils from "../../../../common/utils";


const AddUser = props => {
  const {roles} = props
  return (
    <ManageLayout>
      <div className="add-user">
        <h4 className="mb-4">{Utils.translate('label.add.user')}</h4>
        <Divider type="dashed" className="mb-4"/>
        <UserForm mode="ADD" roles={roles}/>
      </div>
    </ManageLayout>
  )
}
export const getServerSideProps = async (ctx) => {
  const {roleService} = require("server/services")
  const auth = require("server/utils/auth")
  let roles = {};
  try {
    roles = await roleService.queryRoles({}, {page: 1, limit: 10, sortBy: "createdAt"}).catch(err => {
      throw ({
        status: 400,
        message: "Can not load data"
      })
    })
  } catch (err) {
    roles = {}
    const {status, statusCode} = err
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }
  if (JSON.stringify(roles) === "{}") return {props: {}}
  const {results} = roles
  return {
    props: {
      roles: JSON.parse(JSON.stringify(results)),
    }
  }
}
export default AddUser
