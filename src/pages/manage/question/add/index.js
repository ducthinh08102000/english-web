import React, {useState} from "react";
import {Button, Col, Divider, Form, Input, Radio, Row, Select, Tooltip, Grid, notification} from "antd";
import ManageLayout from "common/layouts/manage-layout";
import {QuestionTypeConstant} from "common/constants/QuestionTypeConstant";
import {PlusCircleOutlined} from "@ant-design/icons";
import utils from "../../../../common/utils";
import InputQuestion from "../../../../common/components/manage-components/input-question";
import ApiService from "../../../../common/services/ApiService";
import Utils from "../../../../common/utils";

const {useBreakpoint} = Grid;

const AddQuestion = props => {
  const [questionType, setQuestionType] = useState("doc_hieu");
  const [subCount, setSubCount] = useState(3)
  const isMobile = !utils.getBreakPoint(useBreakpoint()).includes('md');
  const [questions, setQuestions] = useState([]);
  const [form] = Form.useForm();

  const onSubmit = async () => {
    try {
      const res = await ApiService.addExamQuestion({
        question: {
          type: questionType,
          questions: questions
        }
      })
      if (res.status === 200) {
        notification.success({
          message: res.data.message
        })
      }
    } catch (err) {
      console.log(err)
    }

  }

  const changeQuestion = (index, questionHTML) => {
    const newQ = [...questions]
    newQ[index] = questionHTML
    setQuestions(newQ)
  }

  return (
    <ManageLayout>
      <div className="add-exam">
        <h4 className="mb-4">{Utils.translate('label.add.question')}</h4>
        <Divider type="dashed" className="mb-4"/>
      </div>
      <Form
        layout="vertical"
        onFinish={onSubmit}
        form={form}
      >
        <div style={{position: "absolute", top: 42, right: 60}}>
          <Button htmlType="submit" size="small" type="primary" icon={<PlusCircleOutlined/>}>{Utils.translate('label.add.question')}</Button>
        </div>
        <div>
          <div className="d-flex mb-3">
            <div className="choose-question-type mr-3">
              <span className="font-weight-bold mb-3 d-block">{Utils.translate('label.question.type')}</span>
              <Select style={!isMobile ? {width: 200} : {}} defaultValue="doc_hieu"
                      onChange={(value) => setQuestionType(value)}>
                {QuestionTypeConstant?.map((type, index) => (
                  <Select.Option value={type.value} key={type.value + index}>{type.title}</Select.Option>
                ))}
              </Select>
            </div>
            {(questionType === "doc_hieu" || questionType === "dien_tu") && (
              <div className="sub-question">
                <span className="font-weight-bold mb-3 d-block">{Utils.translate('label.sub.question')}</span>
                <Select style={!isMobile ? {width: 200} : {}} defaultValue="3" onChange={(value) => setSubCount(value)}>
                  {[3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map((count, index) => (
                    <Select.Option value={count} key={index}>{count} {Utils.translate('label.sub.question')}</Select.Option>
                  ))}
                </Select>
              </div>
            )}
          </div>
          {["doc_hieu", "dien_tu"].includes(questionType) ? (
            <div>
              <div className="question-paragraph mb-3">
                <InputQuestion form={form} noChoices={true} onChange={(questionHTML) => {
                  changeQuestion(0, questionHTML)
                }}/>
              </div>
              {[...Array(subCount)].map((e, index) => (
                <InputQuestion noChoices={false} key={index + 1} number={index + 1} onChange={(questionHTML) => {
                  changeQuestion(index + 1, questionHTML)
                }}/>
              ))}
            </div>
          ) : (
            <InputQuestion nochoices={false} onChange={(questionHTML) => {
              changeQuestion(0, questionHTML)
            }}/>
          )}
        </div>
      </Form>
    </ManageLayout>
  )
}
export default AddQuestion;
