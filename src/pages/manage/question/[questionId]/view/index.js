import React from "react";
import ManageLayout from "common/layouts/manage-layout";
import RedirectConfig from "common/configs/RedirectConfig";
import {Button, Col, Radio, Row, Tooltip} from "antd";
import Link from "next/link"
import SampleQuestionCard from "common/components/exam-components/sample-question-card";
import Utils from "../../../../../common/utils";


const View = (props) => {
  const {questionData = {}} = props
  const hasChildren = ["doc_hieu", "dien_tu"].includes(questionData?.type) && questionData.parent === 0
  return (
    <ManageLayout>
      <div>
        {hasChildren ? (
          <div>
            <div>
              <div className="d-flex justify-content-between">
                <h4 className="font-weight-semibold">{Utils.translate('label.paragraph')}</h4>
                <Tooltip title="Edit paragraph">
                  <Link href={"/manage/question/" + questionData?.id + "/edit"} passHref>
                    <Button size="small" type="link" ghost>{Utils.translate('label.edit')}</Button>
                  </Link>
                </Tooltip>
              </div>
              <div dangerouslySetInnerHTML={{__html: questionData.data.question}}/>
            </div>
            <div>
              {questionData?.children?.map((question, index) => {
                return (
                  <SampleQuestionCard question={question} index={index} key={question._id}/>
                )
              })}
            </div>
          </div>
        ) : (
          <SampleQuestionCard question={questionData} index={0}/>
        )}
      </div>
    </ManageLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth")
  const {questionService} = require("server/services")
  const {questionId} = ctx.query
  let question = {};
  try {
    await auth(ctx, ["MANAGE_ALL_EXAM_QUESTION", "GET_ALL_EXAM_QUESTION"])
    question = await questionService.getQuestionByFilter({id: questionId})
  } catch (err) {
    question = {};
    const {status, statusCode} = err
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }
  if (JSON.stringify(question) === "{}") return {props: {}}
  return {
    props: {
      questionData: JSON.parse(JSON.stringify(question))
    }
  }
}
export default View
