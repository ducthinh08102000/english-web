import React, {useState} from "react";
import {Button, Divider, notification} from "antd";
import ManageLayout from "common/layouts/manage-layout";
import RedirectConfig from "common/configs/RedirectConfig";
import InputQuestion from "../../../../../common/components/manage-components/input-question";
import {EditOutlined, PlusCircleOutlined} from "@ant-design/icons";
import ApiService from "../../../../../common/services/ApiService";
import Utils from "../../../../../common/utils";


const EditQuestion = props => {
  const {question} = props
  const noChoices = ["dien_tu", "doc_hieu"].includes(question?.type) && question.parent === 0
  const [questionData, setQuestionData] = useState(props.data);
  const [loading, setLoading] = useState(false)
  const changeQuestion = (questionHTML) => {
    setQuestionData(questionHTML)
  }

  const onSubmit = async () => {
    setLoading(true)
    try {
      const res = await ApiService.editExamQuestion(question.id, {
        type: question.type,
        ...questionData
      })
      if (res.status === 200) {
        notification.success({
          message: res.data.message
        })
      }
    } catch (err) {
      console.log(err)
    } finally {
      setLoading(false)
    }
  }

  return (
    <ManageLayout>
      <div className="add-exam">
        <h4 className="mb-4">{Utils.translate('label.edit.question')}</h4>
        <Divider type="dashed" className="mb-4"/>
        <div style={{position: "absolute", top: 42, right: 60}}>
          <Button loading={loading} htmlType="submit" size="small" type="primary" icon={<EditOutlined/>}
                  onClick={onSubmit}>{Utils.translate('label.edit.question')}</Button>
        </div>
        <div>
          <InputQuestion
            number={1}
            questionData={question} mode="EDIT"
            noChoices={noChoices}
            onChange={(questionHTML) => {
              changeQuestion(questionHTML)
            }}
          />
        </div>
      </div>
    </ManageLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const {Question} = require("server/models");
  const auth = require("server/utils/auth");
  const {questionId} = ctx.query;
  let res = {};
  try {
    await auth(ctx, ["MANAGE_ALL_EXAM_QUESTION", "UPDATE_ALL_EXAM_QUESTION"])
    res = await Question.findOne({id: questionId}).catch(err => {
      throw ({
        status: 400,
        message: "Question not found"
      })
    })
  } catch (err) {
    const {status, statusCode} = err;
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }
  if (!res && JSON.stringify(res) === "{}") return {props: {}}
  return {
    props: {
      question: JSON.parse(JSON.stringify(res))
    }
  }

}
export default EditQuestion;
