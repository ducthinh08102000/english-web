import React, {useState} from "react";
import {Divider} from "antd";
import ExamForm from "common/components/manage-components/exam-form";
import ManageLayout from "common/layouts/manage-layout";
import RedirectConfig from "../../../../../common/configs/RedirectConfig";

const EditExam = props => {
  const {exam} = props || {}
  return (
    <ManageLayout>
      <div className="add-exam">
        <h4 className="mb-4">Edit Exam</h4>
        <Divider type="dashed" className="mb-4"/>
        <ExamForm mode="EDIT" exam={exam}/>
      </div>
    </ManageLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth")
  const {examService} = require("server/services");
  const {examCode} = ctx.query;
  let result = {};

  try {
    const {user} = await auth(ctx, ["UPDATE_ALL_EXAM", "MANAGE_ALL_EXAM"]);
    result = await examService.getExamByFilter({code: examCode}, user).catch((err) => {
      throw ({
        message: "Exam not found",
        status: 400
      })
    })
  } catch (err) {
    console.log(err)
    const {status, statusCode} = err;
    return {
      redirect: RedirectConfig(status | statusCode)
    }
  }
  if (!result || JSON.stringify(result) === "{}") return {props: {}}
  const {exam} = result
  return {
    props: {
      exam: JSON.parse(JSON.stringify(exam))
    }
  }
}
export default EditExam;
