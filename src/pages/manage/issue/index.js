import React, {useEffect, useState} from "react";
import {Button, Card, Col, Input, Modal, notification, Pagination, Row, Select, Spin, Table, Tag, Tooltip} from "antd";
import {DeleteOutlined, EditOutlined, EyeOutlined} from "@ant-design/icons";
import ManageLayout from "common/layouts/manage-layout";
import RedirectConfig from "common/configs/RedirectConfig";
import {useIssues} from "common/hooks/useRequest";
import ApiService from "common/services/ApiService";
import CustomModal from "common/components/util-components/CustomModal";
import moment from "moment";
import ReportQuestionContent from "../../../common/components/exam-components/report-question-content";
import ReportResultContent from "../../../common/components/exam-components/report-result-content";
import Utils from "../../../common/utils";

const {Search} = Input

const ManageIssue = props => {
  const [options, setOptions] = useState({page: 1, limit: 10, model: "Question", sortBy: "createdAt"});
  const [shouldFetch, setShouldFetch] = useState(false);
  const [issues, setIssues] = useState(props.issues || [])
  const [pageInfo, setPageInfo] = useState(props.pageInfo || {});
  const {data, loading, error} = useIssues(options, shouldFetch);
  const [visible, setVisible] = useState(false);
  const [selectedIssue, setSelectedIssue] = useState({});
  const [value, setValue] = useState("");
  const [viewIssue, setViewIssue] = useState(false);
  const [model, setModel] = useState("Question")

  useEffect(() => {
    if (data && data.status === 200) {
      const {results, totalResults, page, totalPages} = data.data
      setIssues(results)
      setPageInfo({
        totalResults: totalResults,
        hasNextPage: page < totalPages
      })
      setShouldFetch(false)
    }
    if (error) {
      setShouldFetch(false)
    }
  }, [options.page, data, error])

  const tableColumns = [
    {
      title: Utils.translate('label.question'),
      dataIndex: "target",
      align: "center",
      render: record => {
        return (
          <div>
            <span>{record?.id}</span>
          </div>
        )
      },
    },
    {
      title: Utils.translate('label.announcer'),
      dataIndex: "user",
      align: "center",
      render: record => {
        return (
          <div>
            <span>{record?.username}</span>
          </div>
        )
      },
    },
    {
      title: Utils.translate('label.title'),
      dataIndex: "title",
      width: 120,
      align: "center",
      render: record => {
        return (
          <div>
            <span>{record}</span>
          </div>
        )
      },
    },
    {
      title: Utils.translate('label.issue'),
      dataIndex: "model",
      align: "center",
      render: record => {
        return (
          <div>
            <span>{record}</span>
          </div>
        )
      },
    },
    {
      title: Utils.translate('label.status'),
      dataIndex: "state",
      align: "center",
      render: record => {
        return (
          <div>
            <Tag className="text-capitalize" color={record === "unresolved" ? "red" : "cyan"}>{record}</Tag>
          </div>
        )
      }
    },
    {
      title: Utils.translate('label.options'),
      dataIndex: 'actions',
      align: "center",
      render: (_, record) => (
        <div className="text-right d-flex justify-content-center">
          <Tooltip title={Utils.translate('label.view')}>
            <Button type="primary" className="mr-2" icon={<EyeOutlined/>}
                    onClick={() => {
                      setSelectedIssue(record);
                      setViewIssue(true)
                    }}
                    size="small"/>
          </Tooltip>
          <Tooltip title={Utils.translate('label.edit')}>
            <Button success={"true"} className="mr-2" icon={<EditOutlined/>}
                    onClick={() => {
                      setSelectedIssue(record)
                      setVisible(true)
                    }}
                    size="small"/>
          </Tooltip>
          <Tooltip title={Utils.translate('label.delete')}>
            <Button danger icon={<DeleteOutlined/>}
                    onClick={() => showDeleteConfirm(record['_id'])}
                    size="small"/>
          </Tooltip>
        </div>
      )
    }
  ]


  const resultIssueColumns = [
    {
      title: Utils.translate('label.reported.user'),
      dataIndex: "target",
      align: "center",
      render: record => {
        return (
          <div>
            <span>{record?.user?.username}</span>
          </div>
        )
      },
    },
    {
      title: Utils.translate('label.issue'),
      dataIndex: "model",
      align: "center",
      render: record => {
        return (
          <div>
            <span>{record}</span>
          </div>
        )
      },
    },
    {
      title: Utils.translate('label.correct'),
      dataIndex: "target",
      align: "center",
      render: (_, record) => {
        return (
          <div>
            <span>{_?.nCorrect}/{record?.target?.exam?.totalQuestions}</span>
          </div>
        )
      },
    },
    {
      title: Utils.translate('label.duration'),
      dataIndex: "target",
      align: "center",
      render: record => {
        if (record && record.duration) {
          return (
            <div>
              <span>{moment().startOf("day").seconds(record?.duration).format("HH:mm:ss")}</span>
            </div>
          )
        }
      },
    },
    {
      title: Utils.translate('label.status'),
      dataIndex: "state",
      align: "center",
      render: record => {
        return (
          <div>
            <Tag className="text-capitalize" color={record === "unresolved" ? "red" : "cyan"}>{record}</Tag>
          </div>
        )
      }
    },
    {
      title: Utils.translate('label.options'),
      dataIndex: 'actions',
      align: "center",
      render: (_, record) => (
        <div className="text-right d-flex justify-content-center">
          <Tooltip title={Utils.translate('label.view')}>
            <Button type="primary" className="mr-2" icon={<EyeOutlined/>}
                    onClick={() => {
                      setSelectedIssue(record);
                      setViewIssue(true)
                    }}
                    size="small"/>
          </Tooltip>
          <Tooltip title={Utils.translate('label.edit')}>
            <Button success="true" className="mr-2" icon={<EditOutlined/>}
                    onClick={() => {
                      setSelectedIssue(record)
                      setVisible(true)
                    }}
                    size="small"/>
          </Tooltip>
          <Tooltip title={Utils.translate('label.delete')}>
            <Button danger icon={<DeleteOutlined/>}
                    onClick={() => showDeleteConfirm(record['_id'])}
                    size="small"/>
          </Tooltip>
        </div>
      )
    }
  ]

  const showDeleteConfirm = (issueId) => {
    Modal.confirm({
      title: Utils.translate('label.warn.delete.issue'),
      content: Utils.translate('label.warn.desc.delete.issue'),
      okText: Utils.translate('label.yes'),
      okType: "danger",
      cancelText: Utils.translate('label.no'),
      async onOk() {
        try {
          const res = await ApiService.deleteIssue(issueId);
          if (res.status === 200) {
            const newIssues = [...issues].filter(issue => issue._id !== issueId)
            setIssues(newIssues)
            notification.success({
              message: res.data.message
            })
          }
        } catch (err) {
          console.log(err)
        }
      },
      onCancel() {
        console.log('Cancel');
      }
    })
  }

  const onSubmit = async () => {
    try {
      const res = await ApiService.updateIssue(selectedIssue._id, {state: value})
      if (res.status === 200) {
        const newIssues = [...issues];
        const index = newIssues.findIndex(issue => issue._id === selectedIssue._id)
        if (index !== -1) {
          newIssues[index] = {...newIssues[index], state: res.data.issue.state}
        }
        setSelectedIssue({})
        setIssues(newIssues)
        setVisible(false)
        notification.success({
          message: res.data.message
        })
      }
    } catch (err) {
      console.log(err)
    }
  }

  return (
    <ManageLayout>
      <div className="manage-issue">
        <div className="search-bar mb-4 d-flex">
          <Select style={{width: 150}} className="mr-3" defaultValue="Question" onChange={(value) => {
            setShouldFetch(true)
            setModel(value)
            setOptions({page: 1, limit: 10, model: value, sortBy: "createdAt"})
          }}>
            <Select.Option value="Question">{Utils.translate('label.question.issue')}</Select.Option>
            <Select.Option value="Result">{Utils.translate('label.result.issue')}</Select.Option>
          </Select>
          <Select style={{width: 150}} className="mr-3" defaultValue="all" onChange={(value) => {
            setShouldFetch(true)
            if (value === "all") {
              setOptions({page: 1, limit: 10, model: model, sortBy: "createdAt"})
            } else setOptions({page: 1, limit: 10, model: model, sortBy: "createdAt", state: value})
          }}>
            <Select.Option value="all">{Utils.translate('label.all')}</Select.Option>
            <Select.Option value="unresolved">{Utils.translate('label.unresolved')}</Select.Option>
            <Select.Option value="resolved">{Utils.translate('label.resolved')}</Select.Option>
          </Select>
        </div>
        <Card
          bodyStyle={{'padding': '8px'}}
        >
          <Spin spinning={loading && shouldFetch} tip={Utils.translate('label.loading')}>
            <div className="table-responsive">
              <Table
                columns={options.model === "Question" ? tableColumns : resultIssueColumns} pagination={false}
                dataSource={issues} rowKey='_id'
                footer={() => {
                  return (
                    <Pagination
                      showQuickJumper current={pageInfo?.page || 1} defaultCurrent={pageInfo?.page || 1}
                      total={pageInfo?.totalResults}
                      onChange={(page, pageSize) => {
                        setShouldFetch(true)
                        setOptions({...options, page: page})
                      }}
                    />
                  )
                }}/>
            </div>
          </Spin>
        </Card>
      </div>
      <CustomModal
        width={500} title="Edit issue" visible={visible}
        onCancel={() => {
          setVisible(false)
          setSelectedIssue({})
        }}
        footer={false}
      >
        <div>
          <span className="font-weight-semibold mb-2 d-block">{Utils.translate('label.choose.state.issue')}</span>
          <Select defaultValue={selectedIssue.state} style={{width: 200}} onChange={(v) => setValue(v)}>
            <Select.Option value="unresolved">{Utils.translate('label.unresolved')}</Select.Option>
            <Select.Option value="resolved">{Utils.translate('label.resolved')}</Select.Option>
          </Select>
          <div className="text-right">
            <Button className="mr-2" onClick={() => {
              setVisible(false)
            }}>{Utils.translate('label.cancel')}</Button>
            <Button type="primary" onClick={onSubmit}>OK</Button>
          </div>
        </div>
      </CustomModal>
      <CustomModal
        width={600} footer={null} title={Utils.translate('label.view.issue')} visible={viewIssue}
        onCancel={() => setViewIssue(false)}>
        <div>
          <div>
            <span className="font-weight-semibold">{Utils.translate('label.title')}</span>
            <p>{selectedIssue?.title}</p>
          </div>
          <div>
            <span className="font-weight-semibold">{Utils.translate('label.content')}</span>
            <p>{selectedIssue?.content}</p>
          </div>
          {selectedIssue.model === "Question" ? (
            <ReportQuestionContent data={selectedIssue?.target} more={true}/>
          ) : (
            <ReportResultContent data={selectedIssue?.target} more={true}/>
          )}
        </div>
      </CustomModal>
    </ManageLayout>
  )
}
export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {issueService} = require("server/services");
  let issues = {}
  try {
    const {user} = await auth(ctx, ["MANAGE_ALL_ISSUE"]).catch(err => {
      throw ({
        status: 403,
        message: "You are not authorized to access this page."
      })
    })
    issues = await issueService.queryIssues({model: "Question"}, {
      page: 1,
      limit: 10,
      sortBy: "createdAt"
    }, user.role.permissions.includes('MANAGE_ALL_ISSUE'), user._id)
  } catch (err) {
    issues = {}
    const {status, statusCode} = err
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }
  if (JSON.stringify(issues) === "{}") return {props: {}}
  const {results, totalResults} = issues
  console.log(issues)
  return {
    props: {
      issues: JSON.parse(JSON.stringify(results)),
      pageInfo: {totalResults}
    }
  }
}
export default ManageIssue
