import React, {useCallback, useEffect, useState} from "react";
import {Button, Card, Col, Drawer, Grid, Modal, Result, Row, Spin} from "antd";
import {AppLayout} from "common/layouts/app-layout";
import RedirectConfig from "common/configs/RedirectConfig";
import ExamContent from "common/components/exam-components/exam-content";
import ListQuestion from "common/components/exam-components/list-question";
import {useDispatch, useSelector} from "react-redux";
import ApiService from "common/services/ApiService";
import {resetQuestionState, resetUserAnswers, setLoadingState} from "../../../../common/redux/actions/Exam";
import Link from "next/link";
import useTimers from 'common/hooks/useTimers'
import Icon from "@ant-design/icons";
import {reminderTime} from "common/assets/svg/icon";
import CustomModal from "common/components/util-components/CustomModal";
import utils from "../../../../common/utils";

const {useBreakpoint} = Grid

export const formatTime = timer => {
  const getSeconds = timer ? `0${timer % 60}`.slice(-2) : '00';
  const minutes = `${Math.floor(timer / 60)}`;
  const getMinutes = timer ? `0${minutes % 60}`.slice(-2) : '00';
  const getHours = timer ? `0${Math.floor(timer / 3600)}`.slice(-2) : '00';

  return `${getHours} : ${getMinutes} : ${getSeconds}`;
};


const TakeExam = (props) => {
  const isMobile = !utils.getBreakPoint(useBreakpoint()).includes('md')
  const {exam = {}} = props
  const {userAnswers, loading} = useSelector(state => state['exam'])
  const [visible, setVisible] = useState(false);
  const [resultData, setResultData] = useState({});
  const dispatch = useDispatch();
  const [warning, setWarning] = useState(false);
  const {timer, isOver, isWarning, handleStart, handlePause, handleResume} = useTimers(exam.exam.duration);
  const [showListQuestion, setShowListQuestion] = useState(false);

  const submitExam = async (time) => {
    dispatch(setLoadingState(true))
    let duration = exam.exam.duration - time
    let code = exam.code;
    let submittedAt = new Date().getTime();
    try {
      const res = await ApiService.submitExam(exam.exam.code, {
        submittedAt,
        duration,
        answers: userAnswers,
      })
      if (res.status === 200) {
        const {result} = res.data
        setResultData({
          nCorrect: result?.nCorrect,
          duration: result?.duration,
          examId: result?.exam._id,
          code: result?.exam.code,
          totalQuestion: result.exam?.totalQuestion ?? 50
        })
        dispatch(resetUserAnswers())
        dispatch(resetQuestionState())
        setVisible(true);
      }
    } catch (err) {
      console.log(err)
    } finally {
      dispatch(setLoadingState(false))
    }
  }

  useEffect(() => {
    if (isOver) {
      submitExam(0).then(_ => {
      })
    }
  }, [isOver]) // eslint-disable-line react-hooks/exhaustive-deps

  const onSubmit = () => {
    handlePause()
    Modal.confirm({
      title: 'Do you want to submit this exam?',
      content: "Click OK if you are sure you want to submit. This action can not undo.",
      async onOk() {
        await submitExam(timer);
      },
      onCancel() {
        handleResume()
      },
    });
  }

  const ButtonSubmit = () => (<Button type="primary" className={isMobile && 'mb-3'} block onClick={onSubmit}>Submit</Button>)

  const _renderTimeReminder = () => {
    return (
      <Card
        bordered={!isMobile}
        style={{
          marginBottom: isMobile && 0
        }}
        bodyStyle={{
          padding: isMobile && 0,
          display: isMobile && 'flex',
          alignItems: isMobile && 'center',
        }}
      >
        <div
          className="text-center"
          style={{
            display: isMobile && 'flex',
            alignItems: 'center'
          }}
        >
          {!isMobile && <Icon component={reminderTime} style={{fontSize: isMobile ? 16 : 48}} className={!isMobile ? 'mb-2' : 'mr-2'}/>}
          <h1 style={{fontSize: isMobile && 16, marginBottom: isMobile && 0}}>{formatTime(timer)}</h1>
        </div>
        {!isMobile && <ButtonSubmit/>}
      </Card>
    )
  };

  useEffect(() => {
    handleStart();
  }, [exam.exam.duration]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <AppLayout
      setShowListQuestion={setShowListQuestion}
      timer={isMobile ? _renderTimeReminder() : false}
    >
      <Spin spinning={loading} tip="Processing....">
        <Row gutter={16}>
          <Col xs={24} sm={24} md={24} lg={18} xl={18} xxl={18}>
            <ExamContent data={exam}/>
          </Col>
          <Col xs={24} sm={24} md={24} lg={6} xl={6} xxl={6}>
            <div style={{
              position: "sticky",
              top: "7rem"
            }}>
              {!isMobile && _renderTimeReminder()}
              {isMobile && <ButtonSubmit/>}
              {!isMobile && <ListQuestion setShowListQuestion={false} questions={exam.questions}/>}
            </div>
          </Col>
        </Row>
      </Spin>
      {visible && JSON.stringify(resultData) !== "{}" && (
        <div>
          <Modal width={600} visible={visible} footer={null} closable={false} maskClosable={false}>
            <Result
              status="success"
              title={`You've completed the exam in ${resultData?.duration > 60 ? ~~(resultData?.duration / 60) + " minute(s)" : `${resultData?.duration + " second(s)"}`} and had ${resultData?.nCorrect}/${resultData?.totalQuestion} correct question(s)`}
              subTitle="Success is not final, failure is not fatal: it is the courage to continue that counts"
              extra={[
                <Link href={`/exam/${resultData.code}`} key={0} passHref>
                  <Button type="primary" key="overview">
                    Go overview
                  </Button>
                </Link>,
                <Link href={`/exam/${resultData.code}/result/?id=${resultData.examId}`} key={1} passHref>
                  <Button key="result">View result</Button>
                </Link>,
              ]}
            />
          </Modal>
        </div>
      )}
      {isWarning && (
        <CustomModal visible={isWarning} footer={null} width={500}>
          <Result
            status="warning"
            title="You only have 10 minutes left to finish the exam!"
            extra={
              <div>
                <Button
                  className="mr-2"
                  type="primary" key="console"
                  onClick={() => {
                    handleResume()
                  }}>
                  Continue
                </Button>
                <Button
                  type="default" key="console"
                  onClick={async () => {
                    handlePause()
                    await submitExam(timer)
                  }
                  }
                >
                  Submit exam
                </Button>
              </div>
            }
          />
        </CustomModal>
      )}
      {
        isMobile &&
        <Drawer width={320} placement="left" onClose={() => setShowListQuestion(false)} visible={showListQuestion}>
          <ListQuestion setShowListQuestion={setShowListQuestion} questions={exam.questions}/>
        </Drawer>
      }
    </AppLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {examService} = require("server/services")
  const {examCode} = ctx.query;
  let res = {}
  try {
    const {user} = await auth(ctx);
    res = await examService.getExamByFilter({code: examCode}, user).catch(err => {
      throw ({
        status: 400,
        message: "Exam not found."
      });
    });
  } catch (err) {
    const {status, statusCode} = err
    if (status || statusCode) return {redirect: RedirectConfig(status || statusCode)}
  }
  if (!res || JSON.stringify(res) === "{}") return {props: {}}
  return {
    props: {
      exam: JSON.parse(JSON.stringify(res)),
    }
  }
}
export default TakeExam;
