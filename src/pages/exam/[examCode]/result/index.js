import React from "react";
import {AppLayout} from "common/layouts/app-layout";
import {Col, Row} from "antd";
import ExamContent from "common/components/exam-components/exam-content";
import ListQuestion from "common/components/exam-components/list-question";
import RedirectConfig from "common/configs/RedirectConfig";
import ResultChart from "common/components/exam-components/result-chart";

const Result = (props) => {
  const {result, answersData, exam} = props
  return (
    <AppLayout>
      <Row gutter={16}>
        <Col xs={24} sm={24} md={24} lg={18} xl={18} xxl={18}>
          <ExamContent mode="RESULT" data={exam} answersData={answersData}/>
        </Col>
        <Col xs={24} sm={24} md={24} lg={6} xl={6} xxl={6}>
          <div style={{
            position: "sticky",
            top: "7rem"
          }}>
            <ResultChart result={result}/>
            <ListQuestion questions={exam.questions} mode="RESULT" answersData={answersData}/>
          </div>
        </Col>
      </Row>
    </AppLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {examService} = require("server/services");
  const {Exam} = require("server/models");
  const {examCode} = ctx.query
  let res = {};
  let exam = {}
  try {
    const {user} = await auth(ctx, []);
    exam = await examService.getExamByFilter({code: examCode}, user);
    const examInfo = await Exam.findOne({code: examCode});
    if (!examInfo) {
      throw ({
        status: 400,
        message: "Exam not found"
      })
    }
    res = await examService.getExamResult(examCode, user._id);
  } catch (err) {
    const {status, statusCode} = err
    if (status || statusCode) return {redirect: RedirectConfig(status || statusCode)}
  }
  if (!exam || JSON.stringify(res) === "{}") return {props: {}}
  const {result = {}, answersData = []} = res
  return {
    props: {
      result: JSON.parse(JSON.stringify(result)),
      answersData: JSON.parse(JSON.stringify(answersData)),
      exam: JSON.parse(JSON.stringify(exam))
    }
  }
}
export default Result;
