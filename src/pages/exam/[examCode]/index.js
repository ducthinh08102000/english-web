import React from "react";
import {Alert, Button, Card, Col, List, Row, Tag, Tooltip, Input, Grid} from "antd";
import Icon, {ClockCircleOutlined, CopyOutlined, QuestionCircleOutlined, UserOutlined} from "@ant-design/icons";
import ExamChart from "common/components/exam-components/exam-chart";
import Link from "next/link"
import {examOverview} from "common/assets/svg/icon";
import {AppLayout} from "common/layouts/app-layout";
import RedirectConfig from "common/configs/RedirectConfig";
import moment from "moment";
import CommentCard from "../../../common/components/util-components/comment-card";
import {useRouter} from "next/router";
import {onChangeShowPopup} from "../../../common/redux/actions";
import {useSelector} from "react-redux";
import Utils from "../../../common/utils";

const description = "Tuyển tập các đề thi THPT quốc gia môn tiếng Anh đã được cập nhật. Để làm quen với các dạng bài hay gặp trong đề thi, thử sức với các câu hỏi khó giành điểm 9 – 10 và có chiến lược thời gian làm bài thi phù hợp."
const {useBreakpoint} = Grid;
const ExamOverview = (props) => {
  const isMobile = !Utils.getBreakPoint(useBreakpoint()).includes('md');
  const user = useSelector(state => state.user);
  const router = useRouter();
  const {exam, recommendExam} = props
  return (
    <AppLayout>
      <div className="exam-overview">
        <Row gutter={16}>

          <Col xs={24} sm={24} md={24} lg={{span: 8, order: 2}} xl={8} xxl={8}>
            <Card>
              <h4>Statistic Correct Answers</h4>
              <div>
                <ExamChart/>
                <div className=" mt-3 text-center">
                  <h5 className="mb-0 font-weight-semibold">Medium correct answers: 25</h5>
                </div>
              </div>
            </Card>
            {
              !isMobile &&
              <Card>
                <h4>Recommend Exams</h4>
                <List
                  itemLayout="horizontal"
                  dataSource={recommendExam}
                  renderItem={item => (
                    <List.Item>
                      <List.Item.Meta
                        avatar={<Icon className="mr-2" component={examOverview} style={{fontSize: 42}}/>}
                        title={<a href={`/exam/${item?.code}`}>{item?.name ?? "No data"}</a>}
                        description={<p className="short-description">{description}</p>}
                      />
                    </List.Item>
                  )}
                />
              </Card>
            }
          </Col>
          <Col xs={24} sm={24} md={24} lg={{span: 16, order: 1}} xl={16} xxl={16}>
            <Card title={exam?.name}>
              <div className="d-flex align-items-center mb-3">
                <div>
                  <div className="d-flex mb-3 flex-wrap">
                    <Tooltip title="Total questions" className="mr-3 mb-2">
                      <Tag className="bg-gray-lightest">
                        <QuestionCircleOutlined/>
                        <span className="ml-1">{exam?.totalQuestions} questions</span>
                      </Tag>
                    </Tooltip>
                    <Tooltip title="Duration" className="mr-3 mb-2">
                      <Tag className="bg-gray-lightest">
                        <ClockCircleOutlined/>
                        <span
                          className="ml-1">{moment.utc(moment.duration(exam?.duration, 'seconds').as('milliseconds')).format('HH:mm:ss')}</span>
                      </Tag>
                    </Tooltip>
                    <Tooltip title="Participant" className="mb-2">
                      <Tag className="bg-gray-lightest">
                        <UserOutlined/>
                        <span className="ml-1">{exam?.nParticipants} participants</span>
                      </Tag>
                    </Tooltip>
                    <Tooltip title="Code" className="mb-2">
                      <Tag className="bg-gray-lightest">
                        <CopyOutlined/>
                        <span
                          className="ml-1">{exam?.code}</span>
                      </Tag>
                    </Tooltip>
                  </div>
                  <div>
                    <p className="mb-0">
                      {exam?.description ?? description}
                    </p>
                  </div>
                </div>
              </div>
              {exam?.isDoneBefore && (
                <div>
                  <Alert
                    className="mb-4"
                    message="Hmm..."
                    description="You cannot do this exam twice."
                    type="info"
                    showIcon
                  />
                </div>
              )}
              <div
                className={isMobile && 'd-flex flex-column'}
              >
                <Button style={{marginBottom: isMobile ? 10 : 0}}
                  className="mr-2"
                  type="primary"
                  onClick={() => {
                    if(user._id && user._id !== '') router.push(`/exam/${exam.code}/take-exam`).then(_ => {});
                    else onChangeShowPopup(true);
                  }}
                >
                  Start Exams
                </Button>
                <Link href={`/exam/${exam.code}/result`} passHref>
                  <Button style={{marginBottom: isMobile ? 10 : 0}} className="mr-2" type="default" disabled={!exam?.isDoneBefore}>Results</Button>
                </Link>
                <Link href={`/exam/${exam.code}/ranking`} passHref>
                  <Button className="mr-2" type="default">Ranking</Button>
                </Link>
              </div>
            </Card>
            <Card>
              <CommentCard id={exam._id} model='Exam' title={`Comment(24)`} hasInput={true}
                           placeholderInput='Input comment...'/>
            </Card>
          </Col>
        </Row>
      </div>
    </AppLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const {examService} = require("server/services")
  const {examCode} = ctx.query;
  const auth = require('server/utils/auth');
  const connectDb = require('server/utils/connect-db');
  let res = {}, user = null;
  let recommendExam = [];

  try {
    if(ctx.req.cookies.hasOwnProperty('refresh_token')) {
      const data = await auth(ctx);
      user = data['user'];
    }else await connectDb();
    res = await examService.getExamByFilter({code: examCode}, user).catch(err => {
      console.log(err)
      throw ({
        status: 400,
        message: "Exam not found."
      })
    })
    recommendExam = await examService.queryExams({}, {page: 1, limit: 5});
  } catch (err) {
    const {status, statusCode} = err
    if (status || statusCode) return {redirect: RedirectConfig(status || statusCode)}
  }
  const {results = []} = recommendExam
  const {exam = {}} = res
  return {
    props: {
      exam: JSON.parse(JSON.stringify(exam)),
      recommendExam: JSON.parse(JSON.stringify(results))
    }
  }
}

export default ExamOverview
