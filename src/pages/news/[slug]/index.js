import React from "react";
import {Avatar, Card, Divider, Grid} from "antd";
import {CalendarOutlined, CommentOutlined} from "@ant-design/icons";
import utils from "common/utils";
import CommentCard from "common/components/util-components/comment-card";
import BlogLayout from "common/layouts/blog-layout";
import RedirectConfig from "common/configs/RedirectConfig";
import moment from "moment"
import Utils from "common/utils";
import {useSelector} from "react-redux";

const {useBreakpoint} = Grid;
const BlogPost = props => {
  const {blog} = props;
  const {locale} = useSelector(state => state.theme)
  return (
    <BlogLayout>
      <div className="blog-post">
        <Card bordered={false}>
          <div>
            <div className="begin-info mb-3">
              <div className="d-flex align-items-center">
                <div className="d-flex align-items-center mr-4">
                  <CalendarOutlined className="mr-1"/>
                  <p className="font-weight-semibold mb-0">{moment(parseInt(blog?.createdAt)).format("ll")}</p>
                </div>
                <div className="d-flex align-items-center mr-4">
                  <CommentOutlined className="mr-1"/>
                  <p className="font-weight-semibold mb-0">{blog?.commentCount} {Utils.translate("label.comment")}</p>
                </div>
              </div>
            </div>
            <div className="begin-title">
              <h4 className="mb-3">
                {utils.decodeHtml(blog?.title)}
              </h4>
              <Divider type='dashed' className="mb-3"/>
            </div>
            <div className="content ">
              <div className="mr-2 mb-3" style={{
                backgroundImage: `url(${blog?.thumbnail ?? "https://data.whicdn.com/images/320132508/original.jpg"})`,
                width: "100%",
                height: "30vh",
                backgroundPosition: "center",
                backgroundSize: "cover",
                borderRadius: "0.475rem"
              }}>
              </div>
              <div className="blog-content" dangerouslySetInnerHTML={{__html: blog?.content}}>
              </div>
            </div>
          </div>
        </Card>
        <Card bordered={false}>
          <CommentCard model='Blog' id={blog._id} title={`${Utils.translate("label.comment")}(${blog?.commentCount})`} hasInput={true}/>
        </Card>
      </div>
    </BlogLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {blogService} = require("server/services");
  const {slug} = ctx.query
  let blog = {};
  try {
    blog = await blogService.getBlogByFilter({slug, deleted: {$ne: true}}).catch(err => {
      throw ({
        status: 400,
        message: "Event not found."
      })
    })
  } catch (err) {
    console.log("err", err)
    const {status} = err
    return {
      redirect: RedirectConfig(status)
    }
  }
  if (!blog) return {props: {}}
  return {
    props: {
      blog: JSON.parse(JSON.stringify(blog))
    }
  }
}
export default BlogPost;
