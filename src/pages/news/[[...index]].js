import React, {useState} from "react";
import utils from "common/utils";
import {Grid} from "antd";
import BlogLayout from "common/layouts/blog-layout";
import RedirectConfig from "common/configs/RedirectConfig";
import {loadBlogs} from "common/utils/load-data";
import ApiService from "../../common/services/ApiService";

const {useBreakpoint} = Grid

const Blog = (props) => {
  const {blogs, pageInfo} = props;
  const [blogsData, setBlogsData] = useState(blogs ?? []);
  const [total, setTotal] = useState(pageInfo.totalResults);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);

  const loadMoreBlog = async (nextPage) => {
    setLoading(true);
    try {
      const res = await ApiService.getBlogs({limit: 4, sortBy: '-createdAt', page: nextPage});
      const {totalResults, page, results} = res.data;
      setBlogsData(results);
      setTotal(totalResults);
      setCurrentPage(page);
    } catch (err) {
      console.log(err);
    }
    setLoading(false);
  }

  return (
    <BlogLayout
      propsBlogs={{
        setCurrentPage,
        currentPage,
        loading,
        setLoading,
        loadMoreBlog,
        total,
        pageInfo,
        blogsData
      }}
    />
  )
}


export const getServerSideProps = async (ctx) => {
  let blogs = {};
  try {
    blogs = await loadBlogs(ctx, [])
  } catch (err) {
    const {status} = err
    return {
      redirect: RedirectConfig(status)
    }
  }
  const {results, totalPages, page, totalResults} = blogs
  return {
    props: {
      blogs: JSON.parse(JSON.stringify(results)),
      pageInfo: {
        totalResults, hasNextPage: page < totalPages, total: page < totalPages ? 8 : 0, page,
      }
    }
  }
}
export default Blog
