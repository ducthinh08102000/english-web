import React, {useState} from "react";
import AccountSettingLayout from "common/layouts/setting-account-layout";
import {Button, Divider, Card, Avatar, Skeleton, Tooltip, Spin, notification} from "antd";
import {FacebookSVG, GoogleSVG} from "common/assets/svg/icon";
import GoogleLogin from "react-google-login";
import FacebookLogin from 'react-facebook-login';
import {FacebookOutlined, LogoutOutlined} from "@ant-design/icons";
import {useSelector} from "react-redux";
import ApiService from "../../../common/services/ApiService";
import {updateUser} from "../../../common/redux/actions/User";
import {FACEBOOK_APP_ID, GOOGLE_CLIENT_ID} from "../../../common/configs/AppConfig";

const SSOLogin = props => {
  const [facebookLoading, setFacebookLoading] = useState(false);
  const [googleLoading, setGoogleLoading] = useState(false);
  const user = useSelector(state => state.user);

  const responseFailureGoogle = () => {

  }

  const disConnectSocialNetwork = async (type) => {
    try {
      if (type === 'google') {
        setGoogleLoading(true);
        const res = await ApiService.connectWithGoogle({googleId: user.googleInfo.googleId});
        if (res.data) {
          updateUser(res.data);
          notification.success({message: 'Ngắt liên kết tài khoản Google thành công'});
        }
      } else {
        setFacebookLoading(true);
        const res = await ApiService.connectWithFacebook({facebookId: user.facebookInfo.facebookId});
        if (res.data) {
          updateUser(res.data);
          notification.success({message: 'Ngắt liên kết tài khoản Facebook thành công'});
        }
      }
      setGoogleLoading(false);
      setFacebookLoading(false);
    } catch (err) {
      notification.error({message: 'Ngắt kết nối thất bại'})
    }
  }

  const responseFacebook = async (fbRes) => {
    setFacebookLoading(true);
    const {name, picture, accessToken, userID} = fbRes;
    if (!userID) return notification.error({message: 'Đã có lỗi xảy ra, vui lòng thử lại sau '})
    const data = {
      name: name,
      avatar: picture.data.url,
      accessToken,
    }
    try {
      const res = await ApiService.connectWithFacebook(data);
      if (res.data) {
        updateUser(res.data);
        notification.success({message: 'Liên kết tài khoản Facebook thành công'});
      }
    } catch (err) {
      notification.error({message: 'Liên kết tài khoản Facebook thất bại'})
    }
    setFacebookLoading(false);
  }

  const responseSuccessGoogle = async (data) => {
    setGoogleLoading(true);
    const {accessToken, profileObj} = data;
    try {
      const res = await ApiService.connectWithGoogle({accessToken, name: profileObj.name, avatar: profileObj.imageUrl});
      updateUser(res.data);
      notification.success({message: 'Liên kết tài khoản Google thành công'})
    } catch (err) {
      notification.error({message: 'Liên kết tài khoản Google thất bại'})
    }
    setGoogleLoading(false);
  }


  return (
    <AccountSettingLayout>
      <div className="sso-login">
        <h4 className="mb-4">SSO Login</h4>
        <Divider type="dashed" className="mb-4"/>
        <div
        >
          <div
            className='mb-3'
          >
            {
              user.googleInfo
                ?
                <div>
                  <Spin
                    style={{width: 300}}
                    spinning={googleLoading}
                  >
                    <Card
                      style={{width: 300, marginTop: 16}}
                      bodyStyle={{
                        display: 'flex',
                        padding: 10
                      }}
                    >
                      <Card.Meta
                        className='flex-grow-1 m-0'
                        avatar={<Avatar src={user.googleInfo.avatar}/>}
                        title={
                          <span>
                            {user?.googleInfo?.name ?? ''}
                          </span>
                        }
                        description="Google"
                      />
                      <Tooltip
                        title='Disconnect'
                      >
                        <Button
                          size='small'
                          icon={<LogoutOutlined/>}
                          shape='circle'
                          onClick={() => {
                            disConnectSocialNetwork('google').then(_ => {
                            })
                          }}
                        />
                      </Tooltip>
                    </Card>
                  </Spin>
                </div>
                :
                <div
                  style={{maxWidth: 195, borderRadius: 10, overflow: 'hidden', border: '1px solid rgba(0, 0, 0, 0.1)'}}
                >
                  <Spin
                    spinning={googleLoading}
                  >
                    <GoogleLogin
                      className='button-google-login'
                      clientId={GOOGLE_CLIENT_ID}
                      buttonText="Connect with Google"
                      onSuccess={responseSuccessGoogle}
                      onFailure={responseFailureGoogle}
                      cookiePolicy={'single_host_origin'}
                    />
                  </Spin>
                </div>
            }
          </div>
          <div>
            {
              user.facebookInfo
                ?
                <div>
                  <Spin
                    style={{width: 300}}
                    spinning={facebookLoading}
                  >
                    <Card
                      style={{width: 300, marginTop: 16}}
                      bodyStyle={{
                        display: 'flex'
                      }}
                    >
                      <Card.Meta
                        className='flex-grow-1'
                        avatar={<Avatar src={user.facebookInfo.avatar}/>}
                        title={user?.facebookInfo?.name ??  ''}
                        description="Facebook"
                      />
                      <Tooltip
                        title='Disconnect'
                      >
                        <Button
                          size='small'
                          icon={<LogoutOutlined/>}
                          shape='circle'
                          onClick={() => {
                            disConnectSocialNetwork('facebook').then(_ => {
                            })
                          }}
                        />
                      </Tooltip>
                    </Card>
                  </Spin>
                </div>
                :
                <div
                  style={{maxWidth: 195}}
                >
                  <Spin
                    spinning={facebookLoading}
                  >
                    <FacebookLogin
                      cssClass='button-facebook-login'
                      appId={FACEBOOK_APP_ID}
                      fields="name,email,picture"
                      textButton='Connect with Facebook'
                      callback={responseFacebook}
                      icon={<FacebookOutlined className='mr-2' style={{fontSize: 15}}/>}
                    />
                  </Spin>
                </div>
            }
          </div>
        </div>
      </div>
    </AccountSettingLayout>
  )
}
export default SSOLogin;
