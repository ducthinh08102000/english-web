import React, {useEffect, useState} from "react";
import {Button, Card, Divider} from "antd";
import {PlusCircleOutlined} from "@ant-design/icons";
import {useRouter} from 'next/router';
import QuestionList from "common/components/forum-components/question-list";
import CommunityLayout from "common/layouts/community-layout";
import ApiService from "../../../common/services/ApiService";
import RedirectConfig from "../../../common/configs/RedirectConfig";
import Utils from "../../../common/utils";

const Tags = props => {
  const { favoriteQuestions, popularQuestions } = props;
  const router = useRouter();
  const [total, setTotal] = useState(0);
  const [loading, setLoading] = useState(false);
  const [questions, setQuestions] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    const { results, totalResults } = props.questionData;
    setQuestions(results);
    setCurrentPage(1);
    setTotal(totalResults);
  }, [props.questionData]) // eslint-disable-line react-hooks/exhaustive-deps

  const loadQuestions = async (nextPage) => {
    const { slug } = router.query;
    setLoading(true);
    try {
      const res = await ApiService.getQuestions({limit: 10, page: nextPage, sortBy: '-createdAt', tag: slug});
      const { results, totalResults, page } = res.data;
      setCurrentPage(page);
      setQuestions(results);
      setTotal(totalResults);
    }
    catch(err) {

    }
    setLoading(false);
  }

  const getTitle = () => {
    switch(router.query.slug) {
      case 'exam':
        return 'Exam';
      case 'question':
        return 'Question';
      case 'life_style':
        return 'Life Style';
      case 'learning':
        return 'Learning';
      case 'technology':
        return 'Technology';
      case 'other':
        return 'Other';
    }
  }

  // useEffect(() => {
  //   loadQuestions(1).then(_ => {});
  // }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <CommunityLayout sideData={{popularQuestions, favoriteQuestions}} totalComQuestions={props.totalComQuestions}>
      <Card bordered={false} bodyStyle={{padding: "4px"}}>
        <div className="header-content mb-2">
          <div className="d-flex justify-content-between mb-4">
            <h2 className="mr-2 mb-0">{Utils.translate("label.tag")}: {getTitle()}</h2>
            <Button type="primary" size="small" icon={<PlusCircleOutlined/>} onClick={() => (
              router.replace("/forum/add-question")
            )}>{Utils.translate("label.ask.question")}</Button>
          </div>
          <Divider type="dashed"/>
        </div>
        <div>
          <QuestionList
            {...props}
            setCurrentPage={setCurrentPage}
            currentPage={currentPage}
            setQuestions={setQuestions}
            loading={loading}
            questions={questions}
            totalResults={total}
            loadQuestions={loadQuestions}
          />
        </div>
      </Card>
    </CommunityLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {slug} = ctx.query;
  const connectDb = require("server/utils/connect-db");
  const {communityQuestionService} = require("server/services");
  let data = {};
  try {
    const { favoriteQuestionsData, popularQuestionsData, totalComQuestions, userData } = await Utils.getDataComQuestionLayout(auth, communityQuestionService, ctx, connectDb);
    data['popularQuestions'] = popularQuestionsData?.results ?? [];
    data['favoriteQuestions'] = favoriteQuestionsData?.results ?? [];
    data['totalComQuestions'] = totalComQuestions?.totalResults ?? 0;
    data['questionData'] = await communityQuestionService.queryQuestions({tag: slug, deleted: {$ne: true}}, {limit: 10, page: 1, sortBy: '-createdAt'}, userData?.user?._id ?? null);
  } catch (err) {
    const {status, statusCode} = err;
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }
  const { favoriteQuestions, totalComQuestions, popularQuestions, questionData } = data;
  return {
    props: {
      totalComQuestions,
      favoriteQuestions,
      popularQuestions,
      questionData: JSON.parse(JSON.stringify(questionData))
    }
  }
}

export default Tags;
