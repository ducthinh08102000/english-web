import React, {useEffect, useRef, useState} from 'react'
import CommunityLayout from "common/layouts/community-layout";
import {Avatar, Button, Divider, Input, Card, notification, Result, Tooltip, Form, Skeleton, Select} from "antd";
import {HeartFilled, HeartOutlined, UserOutlined} from "@ant-design/icons";
import moment from "moment";
import ApiService from "../../../common/services/ApiService";
import RedirectConfig from "../../../common/configs/RedirectConfig";
import Link from "next/link";
import Utils from "../../../common/utils";
import ReactMarkdown from "react-markdown";
import Editor from "../../../common/components/util-components/Editor";
import AnswerItem from "../../../common/components/forum-components/Answer";

const Question = (props) => {
  const {favoriteQuestions, popularQuestions, questionData} = props;
  const [isReacted, setIsReacted] = useState(false);
  const [failed, setFailed] = useState(!questionData);
  const [isSaved, setIsSaved] = useState(questionData?.isSaved ?? false);
  const [saving, setSaving] = useState(false);
  const [showFormInput, setShowFormInput] = useState(false);
  const refEditor = useRef(null);
  const [nextPage, setNextPage] = useState(1);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [answers, setAnswers] = useState([]);
  const [loading, setLoading] = useState(false);
  const [sortBy, setSortBy] = useState('-createdAt');

  const loadAnswers = async (currentPage) => {
    try {
      setLoading(true);
      const res = await ApiService.loadComment({
        category: questionData._id,
        limit: 5,
        level: 1,
        page: currentPage,
        sortBy,
      });
      const {results, page, totalPages} = res.data;
      if(page && page === 1) {
        setAnswers(results);
      } else {
        setAnswers(state => [...state, ...results]);
      }
      setNextPage(page + 1);
      setHasNextPage(page < totalPages);
    } catch (err) {
      console.log(err)
    }
    setLoading(false);
  }

  window.onscroll = function () {
    if (window.scrollY + 3 + document.body.offsetHeight >= document.body.scrollHeight) {
      if (!loading && hasNextPage) {
        loadAnswers(nextPage).then(_ => {
        });
      }
    }
  }

  useEffect(() => {
    setAnswers([]);
    loadAnswers(1).then(_ => {
    });
  }, [sortBy])

  const submitAnswer = async () => {
    const value = refEditor.current.getInstance().getMarkdown().replaceAll(/(<br\s*\/*>|\n)/gi, "\n\n");
    if (value === '') {
      return notification.error({message: 'Vui lòng nhập câu trả lời'})
    }
    setLoadingSubmit(true);
    try {
      const res = await Utils.comment({mainId: questionData._id, model: 'CommunityQuestion', content: value});
      const {comment} = res;
      const newComment = {
        ...comment,
        reactionCount: 0,
        reactions: {},
        subCommentCount: 0,
        userReaction: false
      }
      setAnswers(state => [newComment, ...state]);
    } catch (err) {

    }
    setLoadingSubmit(false);
    setShowFormInput(false);
  }

  return (
    <CommunityLayout sideData={{popularQuestions, favoriteQuestions}} totalComQuestions={props.totalComQuestions}>
      {!failed ? <div className="question-overview">
        <Card
          bordered={false}
          bodyStyle={{padding: "4px"}}
        >
          <div className="header-overview mb-4">
            <h3>{Utils.decodeHtml(questionData?.title ?? '')}</h3>
            <div className="description-question">
              <p>
                <div className="question-description mb-3">
                  <p>
                    <ReactMarkdown>
                      {questionData?.content ?? ''}
                    </ReactMarkdown>
                  </p>
                </div>
              </p>
            </div>
          </div>
          <div className="question-footer d-flex justify-content-between align-items-center mb-4">
            <div className="author d-flex align-items-center">
              <div className="author-avatar mr-2">
                <Link href={'/profile/' + questionData?.author?.username ?? ''}>
                  <a>
                    <Avatar src={questionData?.author?.avatar ?? ''} icon={<UserOutlined/>} shape="square"/>
                  </a>
                </Link>
              </div>
              <div className="author-information d-flex flex-column">
                <Link href={'/profile/' + questionData?.author?.username ?? ''}>
                  <a style={{color: '#1a3353'}}>
                    <span className="m-0 under-line"
                          style={{fontSize: "14px", fontWeight: 500}}>{questionData?.author?.displayName ?? ''}</span>
                  </a>
                </Link>
                <span className="m-0 font-size-sm"
                      style={{fontSize: ".8rem"}}>{questionData ? moment(questionData.createdAt).format('MMMM Do YYYY') : ''}</span>
              </div>
            </div>
            <div>
              <Button type="default" size="small" className="mr-2"
                      style={{borderStyle: "dashed"}}>{questionData?.commentCount ?? 0} Answers</Button>
              {
                <Tooltip
                  title={!isSaved ? 'Save question' : 'Unsave question'}
                >
                  <Button
                    type="default"
                    size="small"
                    className="mr-2"
                    style={{borderStyle: "dashed", color: isSaved ? '#3e79f7' : ''}}
                    onClick={async () => {
                      try {
                        if (saving) return;
                        setSaving(true);

                        setIsSaved(state => !state);
                        const res = await ApiService.saveQuestion(questionData.slug);
                        const {message} = res.data;
                        notification.success({message})
                      } catch (err) {
                        setIsSaved(state => state);
                        console.log(err);
                      }
                      setSaving(false);
                    }}
                  >
                    {isSaved ? 'Saved' : 'Save'}
                  </Button>
                </Tooltip>
              }
              {
                isReacted
                  ?
                  <Button
                    onClick={async () => {
                      try {
                        const res = await ApiService.react(questionData._id, {
                          model: 'CommunityQuestion',
                          delete: true
                        });
                        if (res.data) {
                          setIsReacted(false);
                        }
                      } catch (err) {

                      }
                    }}
                    icon={
                      <HeartFilled
                        style={{color: 'red'}}
                      />
                    }
                    size="small"/>
                  :
                  <Button
                    onClick={async () => {
                      try {
                        const res = await ApiService.react(questionData._id, {
                          model: 'CommunityQuestion',
                          active: 'true',
                          type: 'love'
                        });
                        if (res.data) {
                          setIsReacted('love');
                        }
                      } catch (err) {

                      }
                    }}
                    icon={<HeartOutlined/>}
                    size="small"
                  />
              }
            </div>
          </div>
          <Divider type="dashed" className="mb-3"/>
          <div>
            <div className='d-flex justify-content-between align-items-center mb-4'>
              <div className='d-flex flex-grow-1 align-items-center'>
                <h3 className='mb-0 mr-2'>
                  Answers
                </h3>
                {/*<div className='flex-grow-1'>*/}
                  <Select
                    bordered={false}
                    defaultValue='-createdAt'
                    onChange={(value) => {
                      setSortBy(value);
                    }}
                  >
                    <Select.Option value='-createdAt'>
                      Latest answer
                    </Select.Option>
                    <Select.Option value='-reactionCount'>
                      Most helpful answer
                    </Select.Option>
                  </Select>
                {/*</div>*/}
              </div>
              {
                showFormInput
                  ?
                  <Button
                    loading={loadingSubmit}
                    type='primary'
                    size='small'
                    onClick={submitAnswer}
                  >
                    Submit answer
                  </Button>
                  :
                  <Button
                    onClick={() => setShowFormInput(true)}
                    type='primary'
                    size='small'
                  >
                    Add answer
                  </Button>
              }
            </div>
            {
              showFormInput &&
              <div className='mb-4'>
                <Form>
                  <Editor
                    refEditor={refEditor}
                    type='answer'
                  />
                </Form>
              </div>
            }
            <div>
              {
                answers.length > 0 &&
                answers.map(comment => {
                  return (
                    <AnswerItem
                      setAnswer={setAnswers}
                      questionId={questionData._id}
                      key={comment._id}
                      data={comment}
                    />
                  )
                })
              }
            </div>
            {
              loading &&
              <div>
                <Skeleton avatar paragraph={{rows: 2}}/>
                <Skeleton avatar paragraph={{rows: 2}}/>
              </div>
            }
          </div>
        </Card>
      </div> : (
        <Card bordered={false}>
          <Result
            status="404"
            title="400"
            subTitle="Sorry, the page you visited does not exist."
            extra={<Link href="/" passHref>
              <Button type="primary">Back Home</Button>
            </Link>}
          />
        </Card>
      )}
    </CommunityLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const auth = require("server/utils/auth");
  const {questionId} = ctx.query;
  const {communityQuestionService} = require("server/services");
  const connectDb = require('server/utils/connect-db');
  let data = {};
  try {
    const {
      favoriteQuestionsData,
      popularQuestionsData,
      totalComQuestions,
      userData
    } = await Utils.getDataComQuestionLayout(auth, communityQuestionService, ctx, connectDb);
    data['popularQuestions'] = popularQuestionsData?.results ?? [];
    data['favoriteQuestions'] = favoriteQuestionsData?.results ?? [];
    data['totalComQuestions'] = totalComQuestions?.totalResults ?? 0;
    data['questionData'] = JSON.parse(JSON.stringify(await communityQuestionService.getQuestionByFilter({slug: questionId}, userData?.user?._id ?? null)));
  } catch (err) {
    const {status, statusCode} = err;
    return {
      redirect: RedirectConfig(status || statusCode)
    }
  }
  const {favoriteQuestions, totalComQuestions, popularQuestions, questionData} = data;
  return {
    props: {
      totalComQuestions,
      favoriteQuestions,
      popularQuestions,
      questionData,
    }
  }
}

export default Question;
