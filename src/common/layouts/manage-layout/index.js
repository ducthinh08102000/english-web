import React from "react";
import InnerAppLayout from "../inner-app-layout";
import ManageMenuContent from "../../components/manage-components/manage-menu-content";
import {AppLayout} from "../app-layout";

const ManageLayout = props => {
  return (
    <AppLayout>
      <div className="manage-system">
        <InnerAppLayout
          sideContentWidth={320}
          sideContent={<ManageMenuContent {...props} />}
          mainContent={props.children}
        />
      </div>
    </AppLayout>)
}
export default ManageLayout;
