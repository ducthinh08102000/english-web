import React, {useState} from "react"
import {BackTop, Button, Col, Drawer, Grid, Row} from "antd";
import QuestionMenuContent from "common/components/forum-components/question-menu-content";
import SideContent from "common/components/forum-components/side-content";
import {AppLayout} from "../app-layout";
import {ArrowUpOutlined, MenuOutlined} from "@ant-design/icons";
import Utils from "../../utils";

const {useBreakpoint} = Grid;


const SideContentMobile = props => {
  const {sideContent, visible, onSideContentClose} = props
  return (
    <Drawer
      width={320}
      placement="left"
      closable={false}
      onClose={onSideContentClose}
      visible={visible}
      bodyStyle={{paddingLeft: 0, paddingRight: 0}}
    >
      <div className="h-100">
        {sideContent}
      </div>
    </Drawer>
  )
}

const CommunityLayout = props => {
  const {sideData, sideContentGutter = true} = props;
  const isMobile = !Utils.getBreakPoint(useBreakpoint()).includes('lg')
  const [visible, setVisible] = useState(false);

  const close = (e) => {
    setVisible(false)
  }

  const openSideContentMobile = () => {
    setVisible(true)
  }


  return (
    <AppLayout>
      <div className="community-page">
        <Row gutter={16}>
          <Col xs={0} sm={0} md={0} lg={6} xl={5} xxl={5}>
            <QuestionMenuContent totalComQuestions={props.totalComQuestions}/>
          </Col>
          <Col xs={24} sm={24} md={24} lg={11} xl={13} xxl={13}>
            <div>
              {isMobile ?
                <div className={`font-size-lg ${!sideContentGutter ? 'pt-3 px-3' : ''}`}>
                  <MenuOutlined onClick={() => openSideContentMobile()}/>
                </div>
                :
                null
              }
              {props.children}
            </div>
          </Col>
          <Col xs={0} sm={0} md={0} lg={7} xl={6} xxl={6}>
            <SideContent sideData={sideData}/>
          </Col>
        </Row>
      </div>
      <SideContentMobile
        sideContent={<QuestionMenuContent totalComQuestions={props.totalComQuestions}/>}
        visible={visible} onSideContentClose={close}/>
    </AppLayout>
  )
}
export default CommunityLayout;
