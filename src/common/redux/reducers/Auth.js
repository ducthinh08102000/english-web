import {AUTHENTICATED} from "../constants/Auth";

const initState = {}

const auth = (state = initState, action) => {
  switch (action.type) {
    case AUTHENTICATED: {
      return {
        ...state,
        ...action.payload
      }
    }
    default:
      return state;
  }
}
export default auth
