import store from "../store";
import {UPDATE_USER} from "../constants/User";

export const updateUser = (data) => {
  return store.dispatch({
    type: UPDATE_USER,
    payload: data,
  })
}