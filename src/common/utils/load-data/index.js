import auth from "server/utils/auth";
import {blogService, roleService} from "server/services";
const connectDb = require('server/utils/connect-db');

export const loadData = (ctx, permission = [], fn) => {
  return new Promise(async (resolve, reject) => {
    try {
      if(ctx.req.cookies.refresh_token) {
        await auth(ctx, permission);
      } else {
        await connectDb();
      }
      const data = await fn
      resolve(data)
    } catch (err) {
      reject(err)
    }
  })
}

export const loadBlogs = (ctx, permission) => {
  return loadData(ctx, permission, blogService.queryBlogs({}, {limit: 4, page: 1}))
}


export const loadBlog = (ctx, permission, blogId) => {
  const {eventId} = ctx.query
  return loadData(ctx, permission, blogService.getBlogByFilter({_id: eventId}))
}

