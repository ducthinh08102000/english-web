import {
  DashboardOutlined, SettingOutlined, HomeOutlined, ContainerOutlined, DeploymentUnitOutlined, NotificationOutlined
} from '@ant-design/icons';
import {APP_PREFIX_PATH} from 'common/configs/AppConfig'

const dashBoardNavTree = [{
  key: 'ome',
  path: `${APP_PREFIX_PATH}/`,
  title: 'top.nav.home',
  icon: HomeOutlined,
  breadcrumb: false,
  submenu: []
}, {
  key: 'exam',
  path: `${APP_PREFIX_PATH}/exam`,
  title: 'top.nav.exam',
  icon: ContainerOutlined,
  breadcrumb: false,
  submenu: []
}, {
  key: 'news',
  path: `${APP_PREFIX_PATH}/news`,
  title: 'top.nav.news',
  icon: NotificationOutlined,
  breadcrumb: false,
  submenu: []
},
//   {
//   key: 'forum',
//   path: `${APP_PREFIX_PATH}/forum`,
//   title: 'Forum',
//   icon: DeploymentUnitOutlined,
//   breadcrumb: false,
//   submenu: []
// },
  {
    key: 'manage',
    path: `${APP_PREFIX_PATH}/manage/user`,
    title: 'top.nav.manage',
    icon: SettingOutlined,
    breadcrumb: false,
    submenu: []
  }]

const navigationConfig = [
  ...dashBoardNavTree
]

export default navigationConfig;
