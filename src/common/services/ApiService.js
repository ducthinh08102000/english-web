import axios from "axios";
import {API_BASE_URL} from "../configs/AppConfig";
import sendRequest from "./RequestService";


class ApiService {
//auth
  static login(data) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/auth/login`, data).then(res => resolve(res)).catch(err => reject(err));
    })
  }

  static register(data) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/auth/register`, data).then(res => resolve(res)).catch(err => reject(err));
    })
  }

  static refreshToken() {
    return new Promise((resolve, reject) => {
      axios.get(`/api/users/get-self-info`, {}).then(res => resolve(res)).catch(err => reject(err));
    })
  }

  static async logout() {
    return await axios.post("/api/auth/logout", 'post', {});
  }

  //sso login api
  static connectWithGoogle(data) {
    return sendRequest('/api/auth/connect-gg', 'post', data);
  }

  static connectWithFacebook(data) {
    return sendRequest('/api/auth/connect-fb', 'post', data);
  }

  static changePassword(username, data) {
    return sendRequest('/api/users/update/' + username, 'post', data);
  }

  static updateSelfProfile(data) {
    return sendRequest('/api/users/update-self-info', 'post', data);
  }

  //user api
  static getUsers(data, config) {
    return sendRequest("/api/users", "get", data, config)
  }

  static async addUser(data) {
    return await sendRequest("/api/users/add", "post", data)
  }

  static async getUser(userId, config) {
    return await sendRequest("/api/users/" + userId, 'get', {}, config)
  }

  static async deleteUser(username) {
    return await sendRequest("/api/users/delete/" + username, 'post')
  }

  static async updateUser(username, data) {
    return await sendRequest("/api/users/update/" + username, "post", data)
  }

  // exam api
  static async getExams(data, config) {
    return await sendRequest("/api/exams/", "get", data, config)
  }

  static async addExam(data) {
    return await sendRequest("/api/exams/add", "post", data)
  }

  static async deleteExam(examCode) {
    return await sendRequest("/api/exams/delete/" + examCode, 'post', {})
  }

  static async updateExam(examCode, data) {
    return await sendRequest("/api/exams/update/" + examCode, 'post', data)
  }


  static async submitExam(examCode, data) {
    return await sendRequest("/api/exams/submit/" + examCode, 'post', data)
  }

  static async getExam(examCode, config) {
    return await sendRequest("/api/exams/", 'get', examCode, config)
  }

  static getCompletedExams(data) {
    return sendRequest('/api/exams/completed', 'get', data);
  }

  //question api
  static addExamQuestion(data) {
    return sendRequest('/api/questions/add', 'post', data);
  }

  static editExamQuestion(questionId, data) {
    return sendRequest('/api/questions/update/' + questionId, 'post', data);
  }

  static deleteExamQuestion(questionId) {
    return sendRequest('/api/questions/delete/' + questionId, 'post', {});
  }


  // news
  static addBlog(data, config) {
    return sendRequest('/api/blogs/add', 'post', data, config)
  }

  static updateBlog(blogId, data) {
    return sendRequest('/api/blogs/update/' + blogId, 'post', data)
  }

  static deleteBlog(blogs) {
    return sendRequest("/api/blogs/delete-permanently", 'post', blogs)
  }

  static searchBlogs(params) {
    return axios.get('/api/blogs', {params});
  }

  static getBlogs(params) {
    return axios.get('/api/blogs', {params});
  }

  // forum

  static addQuestion(data) {
    return sendRequest('/api/com-questions/add', 'post', data);
  }

  static getQuestions(params) {
    return axios.get('/api/com-questions', {params});
  }

  static getQuestion(slug) {
    return axios.get('/api/com-questions/' + slug);
  }

  static deleteQuestion(id, data) {
    return sendRequest('/api/com-questions/delete/' + id, 'post', data);
  }

  static getTags(data) {
    return sendRequest('/api/com-tags', 'get', data);
  }

  static updateQuestion(id, data) {
    return sendRequest('/api/com-questions/update/' + id, 'post', data);
  }

  static searchQuestion(data) {
    return sendRequest('/api/com-questions', 'get', data);
  }

  static saveQuestion (slug) {
    return sendRequest('/api/com-questions/save/' + slug, 'post', {});
  }

  static getSavedQuestion (params) {
    return sendRequest('/api/com-questions/saved', 'get', params);
  }

  // comment

  static comment(data) {
    return sendRequest('/api/comments/add', 'post', data);
  }

  static loadComment(params) {
    return axios.get('/api/comments', {params});
  }

  static deleteComment (id, data) {
    return sendRequest('/api/comments/delete/' + id, 'post', data);
  }

  static updateComment (id, data) {
    return sendRequest('/api/comments/update/' + id, 'post', data);
  }

  // reaction

  static react(id, data) {
    return sendRequest('/api/reactions/update/' + id, 'post', data);
  }

  // recent activities

  static getRecentActivities(data) {
    return sendRequest('/api/users/activities/', 'get', data);
  }

//  role api
  static addRole(data) {
    return sendRequest('/api/roles/add', 'post', data);
  }

  static deleteRole(slug) {
    return sendRequest('/api/roles/delete/' + slug, 'post', {});
  }

  static updateRole(slug, data) {
    return sendRequest('/api/roles/update/' + slug, 'post', data);
  }

//  issue Api
  static addIssue(data) {
    return sendRequest('/api/issues/add', 'post', data);
  }

  static deleteIssue(issueId) {
    return sendRequest('/api/issues/delete/' + issueId, 'post', {});
  }


  static updateIssue(issueId, data) {
    return sendRequest('/api/issues/update/' + issueId, 'post', data);
  }

  // profile

  static getPosts (params) {
    return axios.get('/api/profile-posts', {params});
  }

  static deletePost (id) {
    return sendRequest('/api/profile-posts/delete/' + id, 'post', {});
  }

  // upload files to discord
  static uploadToDiscord (files) {
    return sendRequest('/api/upload', 'post', {files})
  }

  // resend email
  static verifyEmail () {
    return axios.get('', )
  }

}

export const getLinkVerifyEmail = (email) => {
  return new Promise(async (resolve, reject) => {

    try {
      const res = await axios.post('/api/email/resend', {email});
      resolve(res);
    }
    catch(err) {
      reject(err);
    }
  })
}

export default ApiService;
