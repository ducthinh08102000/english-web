import AntdViVN from 'antd/lib/locale-provider/vi_VN';
import viMsg from "../locales/vi_VN.json";

const ViLang = {
	antd: AntdViVN,
	locale: 'vi-VN',
	messages: {
		...viMsg
	},
};
export default ViLang;
