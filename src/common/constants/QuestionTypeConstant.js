export  const QuestionTypeConstant = [
  {
    value: "doc_hieu",
    title: "Đọc hiểu"
  },
  {
    value: "dien_tu",
    title: "Điền từ"
  }
  ,
  {
    value: "dong_nghia",
    title: "Đồng nghĩa"
  },
  {
    value: "trai_nghia",
    title: "Trái nghĩa"
  },
  {
    value: "ngu_phap",
    title: "Ngữ pháp"
  },
  {
    value: "tim_loi_sai",
    title: "Tìm lỗi sai"
  },
  {
    value: "phat_am",
    title: "Phát âm"
  },
  {
    value: "trong_am",
    title: "Trọng âm"
  },
  {
    value: "doc_hieu",
    title: "Đọc hiểu"
  }, {
    value: "giao_tiep",
    title: "Giao tiếp"
  }
]
