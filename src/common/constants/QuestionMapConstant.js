export const QuestionMapConstant = [
  {
    "type": "trong_am",
    "count": 2
  },
  {
    "type": "phat_am",
    "count": 2
  },
  {
    "type": "dong_nghia",
    "count": 2
  },
  {
    "type": "trai_nghia",
    "count": 2
  },
  {
    "type": "ngu_phap",
    "count": 20
  },
  {
    "type": "giao_tiep",
    "count": 2
  },
  {
    "type": "tim_loi_sai",
    "count": 3
  },
  {
    "type": "dien_tu",
    "count": 5
  },
  {
    "type": "doc_hieu",
    "count": 12
  }
]