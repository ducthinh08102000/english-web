import React, {useState} from 'react';
import Image from 'next/image';
import {Dropdown, Popover} from "antd";

const SelectReaction = (props) => {
  const {handleReact, setShowReaction} = props;
  const reaction = ['like', 'love', 'haha', 'sad', 'wow', 'angry'];

  return (
    <div className='select-reaction' style={{zIndex: 11}}>
      {
        reaction.map(x => {
          return (
            <div
              onClick={() => {
                setShowReaction(false);
                handleReact(x);
              }}
              key={x}
              className='cursor-pointer'
            >
              <div>
                <Image src={`/img/${x}.gif`} width={35} height={35}/>
              </div>
            </div>
          )
        })
      }
    </div>
  )
}

export default SelectReaction;