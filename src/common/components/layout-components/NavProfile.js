import React from "react";
import {Menu, Dropdown, Avatar} from "antd";
import {connect, useSelector} from 'react-redux'
import {
  EditOutlined,
  SettingOutlined,
  LogoutOutlined, UserOutlined
} from '@ant-design/icons';
import Icon from 'common/components/util-components/Icon';
import {signOut} from 'common/redux/actions/Auth';
import {resetToken, setCookiesData} from "../../services/StogareService";
import ApiService from "../../services/ApiService";
import Link from "next/link"
import {updateUser} from "../../redux/actions/User";

export const NavProfile = () => {
  const user = useSelector(state => state.user);

  const menuItem = [
    {
      title: "View Profile",
      icon: EditOutlined,
      path: "/profile/" + user.username
    },

    {
      title: "Account Setting",
      icon: SettingOutlined,
      path: "/account-setting/edit-profile/"
    },
  ]

  const signOut = async () => {
    try {
      const res = await ApiService.logout();
      if (res.status === 200) {
        setCookiesData('prePath', '/', 0);
      }
      updateUser({});
    } catch (err) {
      console.log(err)
    }
    resetToken();
    await window.location.replace('/auth/login');
  }
  const profileMenu = (
    <div className="nav-profile nav-dropdown">
      <div className="nav-profile-header">
        <div className="d-flex">
          <Avatar size={45} src={user.avatar} shape="square" icon={<UserOutlined/>}/>
          <div className="pl-3">
            <h4 className="mb-0">{user?.fullName}</h4>
            <span className="text-muted">@{user?.username}</span>
          </div>
        </div>
      </div>
      <div className="nav-profile-body">
        <Menu>
          {menuItem.map((el, i) => {
            return (
              <Menu.Item key={i}>
                <Link href={el.path}>
                  <a style={{display: 'block', height: '100%'}}>
                    <Icon className="mr-3" type={el.icon}/>
                    <span className="font-weight-normal">{el.title}</span>
                  </a>
                </Link>
              </Menu.Item>
            );
          })}
          <Menu.Item key={menuItem.length + 1} onClick={e => signOut()}>
            <span>
              <LogoutOutlined className="mr-3"/>
              <span className="font-weight-normal">Sign Out</span>
            </span>
          </Menu.Item>
        </Menu>
      </div>
    </div>
  );
  return (
    <Dropdown placement="bottomRight" overlay={profileMenu} trigger={["click"]}>
      <Menu className="d-flex align-item-center" mode="horizontal">
        <Menu.Item

        >
          <Avatar src={user.avatar} icon={<UserOutlined/>} shape="square"/>
        </Menu.Item>
      </Menu>
    </Dropdown>
  );
}

export default connect(null, {signOut})(NavProfile)
