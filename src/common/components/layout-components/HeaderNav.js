import React, {useState, useEffect} from "react";
import {connect, useSelector} from "react-redux";
import {Button, Grid, Layout, Tag, Tooltip} from "antd";
import {MenuFoldOutlined, MenuUnfoldOutlined} from '@ant-design/icons';
import Logo from './Logo';
import NavSearch from './NavSearch';
import {toggleCollapsedNav, onMobileNavToggle} from 'common/redux/actions/Theme';
import {NAV_TYPE_TOP, SIDE_NAV_COLLAPSED_WIDTH, SIDE_NAV_WIDTH} from 'common/constants/ThemeConstant';
import utils from 'common/utils'
import CustomMenuContent from "./custom-menu-content";
import {NavProfile} from "./NavProfile";
import Link from 'next/link';
import {NavLanguage} from "./NavLanguage";

const {Header} = Layout;
const {useBreakpoint} = Grid;

export const HeaderNav = props => {
  const screens = utils.getBreakPoint(useBreakpoint());
  const isXs = !screens.includes('sm');
  const {
    navCollapsed,
    mobileNav,
    navType,
    headerNavColor,
    toggleCollapsedNav,
    onMobileNavToggle,
    isMobile,
    currentTheme,
    direction
  } = props;
  const [searchActive, setSearchActive] = useState(false)
  const user = useSelector(state => state.user);

  const onSearchClose = () => {
    setSearchActive(false)
  }

  const onToggle = () => {
    if (!isMobile) {
      toggleCollapsedNav(!navCollapsed)
    } else {
      onMobileNavToggle(!mobileNav)
    }
  }

  const isNavTop = navType === NAV_TYPE_TOP
  const mode = () => {
    if (!headerNavColor) {
      return utils.getColorContrast(currentTheme === 'dark' ? '#00000' : '#ffffff')
    }
    return utils.getColorContrast(headerNavColor)
  }
  const navMode = mode()
  const getNavWidth = () => {
    if (isNavTop || isMobile) {
      return '0px'
    }
    if (navCollapsed) {
      return `${SIDE_NAV_COLLAPSED_WIDTH}px`
    } else {
      return `${SIDE_NAV_WIDTH}px`
    }
  }

  useEffect(() => {
    if (!isMobile) {
      onSearchClose()
    }
  })

  return (
    <Header className={`app-header ${navMode}`} style={{backgroundColor: headerNavColor}}>
      <div className={`app-header-wrapper ${isNavTop ? 'layout-top-nav' : ''}`}>
        <Logo logoType={navMode}/>
        <div className="nav" style={{width: `calc(100% - ${getNavWidth()})`}}>
          <div className="nav-left">
            <ul className="ant-menu ant-menu-root ant-menu-horizontal w-100">
              {
                isNavTop && !isMobile ?
                  <CustomMenuContent/>
                  :
                  <>
                    <li className="ant-menu-item ant-menu-item-only-child" onClick={() => {
                      onToggle()
                    }}>
                      {navCollapsed || isMobile ? <MenuUnfoldOutlined className="nav-icon"/> :
                        <MenuFoldOutlined className="nav-icon"/>}
                    </li>
                    {
                      props.timer && isMobile &&
                      <Button
                        size='small'
                        type='primary'
                        className='ml-3'
                        onClick={() => {
                          if (props.setShowListQuestion) {
                            props.setShowListQuestion(true);
                          }
                        }}
                      >
                        {isXs ? '?' : 'Question List'}
                      </Button>
                    }
                  </>
              }
            </ul>
          </div>
          <div className="nav-right">
            <NavLanguage />
            {user._id && user._id !== ''
              ?
              <div
                className='d-flex align-items-center'
              >
                {props.timer}
                <NavProfile/>
              </div>
              :
              <div>
                <Link href='/auth/login'>
                  <a>
                    <Button type='primary' className='mr-2'>
                      Sign in
                    </Button>
                  </a>
                </Link>
                <Link href='/auth/register'>
                  <a>
                    <Button>
                      Register
                    </Button>
                  </a>
                </Link>
              </div>
            }
          </div>
          <NavSearch active={searchActive} close={onSearchClose}/>
        </div>
      </div>
    </Header>
  )
}

const mapStateToProps = ({theme}) => {
  const {navCollapsed, navType, headerNavColor, mobileNav, currentTheme, direction} = theme;
  return {navCollapsed, navType, headerNavColor, mobileNav, currentTheme, direction}
};

export default connect(mapStateToProps, {toggleCollapsedNav, onMobileNavToggle})(HeaderNav);
