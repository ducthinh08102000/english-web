import React, {useEffect, useState} from "react";
import {Button, Col, DatePicker, Form, Input, notification, Row, Select} from "antd";
import {ROW_GUTTER} from "../../../constants/ThemeConstant";
import ApiService from "../../../services/ApiService";
import DynamicSelect from "../../util-components/DynamicImport/dynamic-select";
import moment from "moment";
import {useSelector} from "react-redux";
import {useRouter} from "next/router";
import Utils from "../../../utils";

const dateFormat = "DD-MM-YYYY";
const {Option} = Select;
const {TextArea} = Input

const ADD = "ADD";
const EDIT = "EDIT";
const VIEW = "VIEW"

const UserForm = props => {
  const userData = useSelector(state => state.user);
  const {mode = ADD, param, user = {}, roles = []} = props;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [verifyEmailLoading, setVerifyEmailLoading] = useState(false);

  const router = useRouter();

  useEffect(() => {
    if (mode === EDIT && JSON.stringify(user) !== "{}") {
      form.setFieldsValue({
        ...user,
        dob: user.dob ? moment(parseInt(user.dob)) : undefined,
        role: user?.role.slug
      })
    }
  }, [user])


  const onSubmit = async values => {
    setLoading(true)
    if (mode === ADD) {
      try {
        const res = await ApiService.addUser({
          ...values,
          dob: values.dob.format("x")
        })
        if (res.status === 200) {
          form.resetFields();
          notification.success({
            message: res.data.message
          })
        }
      } catch (err) {
        console.log(err)
      }
    }
    if (mode === EDIT) {
      try {
        let res = user.username === userData.username ? await ApiService.updateSelfProfile({
          ...values,
          dob: values.dob.format("x")
        }) : await ApiService.updateUser(user.username, {...values, dob: values.dob.format("x")})
        if (res.status === 200) {
          notification.success({
            message: res.data.message
          })
        }
      } catch (err) {
        console.log(err)
      }
    }
    setLoading(false)
  }

  const verifyEmail = async () => {
    setVerifyEmailLoading(true);
    try {
      const res = await ApiService.verifyEmail();
      console.log(res);
    }
    catch(err) {

    }
    setVerifyEmailLoading(false);
  }

  return (
    <Form
      className="user-form"
      layout="vertical"
      onFinish={onSubmit}
      form={form}
    >
      <Row gutter={ROW_GUTTER}>
        <Col xs={24} sm={24} md={12}>
          <Form.Item
            label={Utils.translate('label.username')}
            name="username"
            rules={[
              {
                required: true,
                message: Utils.translate('label.warn.input.username')
              },
            ]}
          >
            <Input disabled={mode === EDIT} placeholder={Utils.translate('label.input.username')}
                   className={mode === VIEW ? "readOnly" : ""}/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={12}>
          <Row className='align-items-center'
          >
            <Col className='flex-grow-1 mr-2'>
              <Form.Item
                className='d-flex'
                label="Email"
                name="email"
                rules={[{
                  required: true,
                  type: 'email',
                  message: Utils.translate('label.warn.input.email')
                }]}
              >
                <Input placeholder={Utils.translate('label.input.email')} disabled={mode === EDIT} className={mode === VIEW ? "readOnly" : ""}/>
              </Form.Item>
            </Col>
            {
              user._id === userData._id && !userData.isVerifiedEmail &&
              <Col>
                <Button
                  loading={verifyEmailLoading}
                  onClick={verifyEmail}
                  type='primary'
                >
                  Verify
                </Button>
              </Col>
            }
          </Row>
        </Col>
      </Row>
      <Row gutter={ROW_GUTTER}>
        <Col xs={24} sm={24} md={12}>
          <Form.Item
            label={Utils.translate('label.full.name')}
            name="fullName"
            rules={[
              {
                required: true,
                message: Utils.translate('label.warn.input.full.name')
              },
            ]}
          >
            <Input placeholder={Utils.translate('label.input.full.name')} className={mode === VIEW ? "readOnly" : ""}/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={12}>
          <Form.Item
            label={Utils.translate('label.display.name')}
            name="displayName"
          >
            <Input placeholder={Utils.translate('label.input.display.name')} className={mode === VIEW ? "readOnly" : ""}/>
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={ROW_GUTTER}>
        <Col xs={24} sm={24} md={12}>
          <Form.Item
            name="dob"
            label={Utils.translate('label.dob')}
            rules={[
              {
                required: true,
                message: 'Please input your birthday!',
              },
            ]}
          >
            <DatePicker style={{width: "100%"}} format={dateFormat} className={mode === VIEW ? "readOnly" : ""}/>
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={12}>
          <Form.Item
            name="gender"
            label={Utils.translate('label.gender')}
            placeholder="Select gender"
            rules={[
              {
                required: true,
                message: Utils.translate('label.warn.select.gender'),
              },
            ]}
          >
            <Select placeholder={Utils.translate('label.select.gender')} className={mode === VIEW ? "readOnly" : ""}>
              <Option value="1">{Utils.translate('label.male')}</Option>
              <Option value="2">{Utils.translate('label.female')}</Option>
              <Option value="3">{Utils.translate('label.other')}</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
      {['UPDATE_ALL_USER', "MANAGE_ALL_USER"].some(per => userData?.role?.permissions.includes(per)) && router.pathname.includes("/manage/user/") &&
      (<Row gutter={ROW_GUTTER}>
          <Col xs={24} sm={24} md={24}>
            <Form.Item
              name="role"
              label={Utils.translate('label.role')}
            >
              <Select placeholder={Utils.translate('label.choose.role')}>
                {roles.map((role, index) => (
                  <Select.Option value={role?.slug} key={index}>
                    <span className="text-capitalize">{role?.name}</span>
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>
      )
      }
      <Row gutter={ROW_GUTTER}>
        <Col xs={24} sm={24} md={24}>
          <Form.Item
            label={Utils.translate('label.biography')}
            name="bio"
          >
            <TextArea rows={4} placeholder={Utils.translate('label.input.biography')} className={mode === VIEW ? "readOnly" : ""}/>
          </Form.Item>
        </Col>
      </Row>
      {userData?.role?.name === 'admin' && router.pathname.includes("/manage/user/") && (
        <Form.Item
          name="password"
          label={mode === ADD ? Utils.translate('label.password') : Utils.translate('label.password.if.want')}
          rules={mode === ADD ? [{
            required: true,
            message: Utils.translate('label.warn.input.password')
          }] : []}
        >
          <Input.Password placeholder={Utils.translate('label.input.password')}/>
        </Form.Item>
      )}
      {/*{mode === ADD && userData?.role?.name === 'admin' && (*/}
      {/*  <Form.Item*/}
      {/*    name="confirmPassword"*/}
      {/*    label="Confirm Password"*/}
      {/*    rules={[{*/}
      {/*      required: true,*/}
      {/*      message: "Please input your password"*/}
      {/*    }]}*/}
      {/*  >*/}
      {/*    <Input.Password placeholder="Input confirm password..."/>*/}
      {/*  </Form.Item>*/}
      {/*)}*/}
      {mode !== VIEW && (
        <Button type="primary" htmlType="submit" style={{float: "right"}} loading={loading}>
          {mode === ADD ? "Add User" : "Save Change"}
        </Button>
      )}
    </Form>
  )
}


export default UserForm;
