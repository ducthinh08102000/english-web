import React, {createRef, useEffect, useRef, useState} from "react";
import {Button, Col, Form, Input, notification, Row, Select, Upload, Grid} from "antd";
import {InboxOutlined} from "@ant-design/icons";
import ApiService from "../../../services/ApiService"
import EditorConfig from "../../../configs/EditorConfig";
import dynamic from "next/dynamic";
import {categories} from "../../../constants/BlogCategory";
import Utils from "../../../utils";

const ReactQuill = dynamic(() => import('react-quill'), {ssr: false});

const ADD = "ADD";
const EDIT = "EDIT"


const BlogForm = props => {
  const {mode = ADD} = props;
  const [form] = Form.useForm();
  const [blog, setBlog] = useState(props.blog || {});
  const [loading, setLoading] = useState(false)
  const [imagePreview, setImagePreview] = useState(null)
  const ref = createRef();

  useEffect(() => {
    if (mode === EDIT) {
      setImagePreview(blog.thumbnail)
      form.setFieldsValue({
        ...blog
      })
    }
  }, [props.blog])

  const onSubmit = async (values) => {
    setLoading(true)
    const formData = new FormData();
    for (const value in values) {
      if (value === "file") {
        values[value] && formData.append(value, values[value].originFileObj);
        continue;
      }
      formData.append(value, values[value])
    }

    try {
      let res = {}
      if (mode === ADD) {
        res = await ApiService.addBlog(formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
          }
        })
        if (res.status === 200) {
          form.resetFields();
          setImagePreview(null);
          notification.success({
            message: res.data.message
          })
        }
      }
      if (mode === EDIT) {
        res = await ApiService.updateBlog(blog.slug, formData, {
          'Content-Type': 'multipart/form-data',
        })
        if (res.status === 200) {
          notification.success({
            message: res.data.message
          })
        }
      }
    } catch (err) {
      console.log(err)
    } finally {
      setLoading(false)
    }
  }

  const onChange = async (e) => {
    let file = e.file;
    let src = file.url;
    if (!src) {
      src = await new Promise(resolve => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    setImagePreview(src)
  };

  return (
    <Form
      layout="vertical"
      onFinish={onSubmit}
      form={form}
    >
      <Row gutter={16}>
        <Col sm={24} md={16} lg={18}>
          <Form.Item
            name="title"
            label={Utils.translate('label.post.title')}
            rules={[{required: true, message: Utils.translate('label.warn.input.post.title')}]}
          >
            <Input placeholder={Utils.translate('label.input.post.title')}/>
          </Form.Item>
          <Form.Item initialValue='' className="mt-4" name='content' getValueFromEvent={e => {
            return e;
          }} label={Utils.translate('label.news.content')}>
            <ReactQuill value='' theme="snow" modules={EditorConfig} ref={ref}/>
          </Form.Item>
        </Col>
        <Col sm={24} md={8} lg={6}>
          <Form.Item name="categories" label={Utils.translate('label.post.category')}>
            <Select
              mode="multiple"
              style={{width: '100%'}}
              placeholder={Utils.translate('label.select.category')}
            >
              {categories && categories.map((category, index) => {
                return <Select.Option key={category.slug + index} value={category.slug}>{category.title}</Select.Option>
              })}
            </Select>
          </Form.Item>
          <Form.Item label={Utils.translate('label.post.image')}>
            <Form.Item
              name="file" valuePropName="file"
              getValueFromEvent={(e) => {
                if (Array.isArray(e)) return e;
                return e && e.file;
              }}
              noStyle
            >
              <Upload.Dragger
                maxCount={1}
                showUploadList={false}
                onChange={onChange}
              >
                {imagePreview ? (
                  <img src={imagePreview} alt="avatar" className="img-fluid"/>
                ) : (
                  <div>
                    <p className="ant-upload-drag-icon">
                      <InboxOutlined/>
                    </p>
                    <p className="ant-upload-text">{Utils.translate('label.click.or.drag')}</p>
                  </div>
                )}
              </Upload.Dragger>
            </Form.Item>
          </Form.Item>
          <Form.Item>
            <Button loading={loading} type="primary" htmlType="submit"
                    block>{mode === ADD ? Utils.translate('label.create.post') : Utils.translate('label.save.post')}</Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>)
}
export default BlogForm;
