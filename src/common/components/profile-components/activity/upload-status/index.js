import React, {useState} from "react";
import {Avatar, Button, Card, Form, Input, Modal, notification, Select, Spin, Upload} from "antd";
import {InboxOutlined, UserOutlined} from "@ant-design/icons";
import axios from "axios";
import {useRouter} from "next/router";
import {useSelector} from "react-redux";

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

const UploadStatusCard = props => {
  const user = useSelector(state => state.user);
  const router = useRouter();
  const [visible, setVisible] = useState(false);
  const [form] = Form.useForm();
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState(null);
  const [file, setFile] = useState(null);
  const [loading, setLoading] = useState(false);

  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }
    setPreviewImage(file.url || file.preview);
    // setPreviewVisible(true);
  }

  const handlePost = async (values) => {
    const { userId } = router.query;
    setLoading(true);
    const { content, privacy } = values;
    const data = new FormData();
    data.append('content', content);
    data.append('privacy', privacy);
    data.append('location', userId);
    if(file) data.append('file', file);
    try {
      const res = await axios.post('/api/profile-posts/add', data, {
        headers: {
          'Content-Type': 'multipart/form-data',
        }
      })
      const { post } = res.data;
      const postData = {...post, userReaction: false, commentCount: 0, reaction: {}, reactionCount: 0};
      props.updatePostList(state => [postData, ...state]);
      notification.success({message: 'Create post successfully'});
    }
    catch(err) {
      notification.error({message: 'Failed to post story'});
    }
    setPreviewImage(null);
    setVisible(false);
    setFile(null);
    form.resetFields();
    setLoading(false);
  }

  return (
    <Card bordered={false}>
      <h4 className="mb-3">Activity</h4>
      <div className="upload-status">
        <div className="d-flex align-items-start">
          <div className="avatar mr-2">
            <Avatar src={user.avatar} shape="square" icon={<UserOutlined/>}/>
          </div>
          <div className="flex-grow-1">
            <Input
              placeholder="Input status..."
              onClick={() => {
                // notification.warn({message: 'This feature is in development.'})
                setVisible(true);
              }}
            />
          </div>
        </div>
      </div>
      <Modal
        onCancel={() => setVisible(false)}
        visible={visible}
        footer={false}
      >
        <Spin
          spinning={loading}
          tip='Creating post...'
        >
          <Form
            onFinish={handlePost}
            form={form}
            layout='vertical'
          >
            <Form.Item
              name='content'
              label='Content'
              rules={[
                {required: true, message: 'Please enter content...'}
              ]}
            >
              <Input.TextArea placeholder='Enter content...'/>
            </Form.Item>
            <Form.Item
              name='privacy'
              label='Privacy'
              rules={[{required: true, message: 'Please select privacy'}]}
            >
              <Select placeholder='Select privacy'>
                <Select.Option key='private' value='public'>
                  Public
                </Select.Option>
                <Select.Option key='private' value='private'>
                  Private
                </Select.Option>
                {/*<Select.Option key='private' value='friends'>*/}
                {/*  Friends*/}
                {/*</Select.Option>*/}
              </Select>
            </Form.Item>
          </Form>
          <div
            className='d-flex justify-content-between'
          >
            <div style={{color: '#1A3353', fontSize: 14, marginBottom: 8, fontWeight: 500}}>
              Add images:
            </div>
            <Button
              size='small'
              type='link'
              onClick={() => {
                setFile(null);
                setPreviewImage(null);
              }}
            >
              Delete image
            </Button>
          </div>
          {/*<Upload*/}
          {/*  fileList={fileList}*/}
          {/*  listType="picture-card"*/}
          {/*  onChange={(files) => {*/}
          {/*    const {fileList} = files;*/}
          {/*    setFileList(fileList);*/}
          {/*    return true;*/}
          {/*  }}*/}
          {/*  onPreview={handlePreview}*/}
          {/*>*/}
          {/*  {fileList.length === 0 && <div>*/}
          {/*    <PlusOutlined/>*/}
          {/*    <div style={{marginTop: 8}}>Upload</div>*/}
          {/*  </div>}*/}
          {/*</Upload>*/}
          <Upload.Dragger
            className="mb-3"
            maxCount={1}
            showUploadList={false}
            onChange={(files) => {
              const imageType = ['image/jpeg', 'image/png'];
              if(!imageType.includes(files.file.type)) {
                return notification.error({message: 'Only accept files with extension .png, .jpg'});
              }
              setFile(files.file.originFileObj);
              handlePreview(files.file).then(_ => {});
            }}
          >
            {
              previewImage
                ?
                <img src={previewImage} alt="avatar" className="img-fluid"/>
                :
                <div>
                  <p className="ant-upload-drag-icon">
                    <InboxOutlined/>
                  </p>
                  <p className="ant-upload-text">Click/drag image file here.</p>
                </div>
            }
          </Upload.Dragger>
          <Modal
            visible={previewVisible}
            footer={null}
            onCancel={() => {
              setPreviewVisible(false);
            }}
          >
            {/* eslint-disable-next-line @next/next/no-img-element */}
            <img alt="example" style={{ width: '100%' }} src={previewImage} />
          </Modal>
          <div
            className='d-flex justify-content-end'
          >
            <Button size='small' onClick={() => setVisible(false)}>
              Cancel
            </Button>
            <Button
              className='ml-2'
              type='primary'
              size='small'
              onClick={() => {form.submit()}}
            >
              Finish
            </Button>
          </div>
        </Spin>
      </Modal>
    </Card>
  )
}
export default UploadStatusCard;
