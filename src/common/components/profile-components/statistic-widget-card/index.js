import React from "react";
import {ArrowDownOutlined, ArrowUpOutlined} from "@ant-design/icons";
const StatisticWidgetCard = ({title, value, status, subtitle, prefix, className}) => {
  return (
    <div className="custom-widget">
      <div className={`statistic-widget-card  border border-dashed ${className ? className : ""}`}>
        <div className={`${prefix ? 'd-flex' : ''} ${title ? 'mt-1' : ''}`}>
          {prefix ? <div className="mr-2">{prefix}</div> : null}
          <div>
            <div className="d-flex align-items-center">
              <h4 className="mb-0 font-weight-bold">{value}</h4>
              {
                status ?
                  <span
                    className={`font-size-sm font-weight-bold ml-2 ${status !== 0 && status > 0 ? 'text-success' : 'text-danger'}`}>
								{status}
                    {status !== 0 && status > 0 ? <ArrowUpOutlined/> : <ArrowDownOutlined/>}
							</span>
                  :
                  null
              }
            </div>
            {subtitle && <div className="text-gray-light mt-1">{subtitle}</div>}
          </div>
        </div>
        {title && <h5 className="text-gray-light mb-0">{title}</h5>}
      </div>
    </div>
  )
}
export default StatisticWidgetCard;
