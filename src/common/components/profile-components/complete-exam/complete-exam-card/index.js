import React from "react";
import {Card} from "antd";
import Link from 'next/link';
const CompleteExamCard = props => {
  const { data } = props;
  return (
    <div className="exam-card">
      <Link href={'/exam/' + data?.exam?.code}>
        <a>
          <Card
            size="small"
            className="hover custom-card"
            hoverable
            // cover={
            //   <div>
            //     <div className="text-center p-3" style={{
            //       borderTopLeftRadius: ".625rem",
            //       borderTopRightRadius: ".625rem",
            //       backgroundImage: `url('img/others/img-24.jpg')`,
            //       backgroundRepeat: 'no-repeat',
            //       backgroundPosition: 'bottom center',
            //       backgroundSize: "70%",
            //       height: 200
            //     }}>
            //     </div>
            //   </div>
            // }
          >
            <div className="text-center p-3" style={{
              borderTopLeftRadius: ".625rem",
              borderTopRightRadius: ".625rem",
              backgroundImage: `url('/img/others/img-24.jpg')`,
              backgroundRepeat: 'no-repeat',
              backgroundPosition: 'bottom center',
              backgroundSize: "70%",
              height: "20vh"
            }}>
            </div>
            <Card.Meta
              title={data?.exam?.name ?? ''}
              description={<p className="short-description">{data?.exam?.description ?? ''}</p>}
            />
          </Card>
        </a>
      </Link>
    </div>
  )
}
export default CompleteExamCard;
