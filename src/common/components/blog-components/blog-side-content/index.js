import React, {useEffect, useRef, useState} from "react";
import {Card, Input, Typography} from "antd";
import {SearchOutlined} from "@ant-design/icons";
import ApiService from "../../../services/ApiService";
import moment from "moment";
import Link from 'next/link';
import {useRouter} from "next/router";
import Utils from "../../../utils";

const BlogSideContent = props => {
  const { parentElm } = props;
  const router = useRouter();
  const {eventId} = router.query;
  const {searchBlogs, keyword, setKeyword, setCurrentPageSearch, setTotalResultSearch} = props;
  const [recentBlog, setRecentBlog] = useState([]);
  const ref = useRef(null);

  useEffect(() => {
    (async () => {
      try {
        const res = await ApiService.getBlogs({limit: 10, page: 1, sortBy: '-createdAt'});
        const {results} = res.data;
        setRecentBlog(results);
      } catch (err) {
        console.log(err);
      }
    })()
  }, [])

  return (
    <Card
      bordered={false}
      style={{
        position: 'fixed',
        width: parentElm.current ? parentElm.current.offsetWidth : 0,
      }}
    >
      <div className='overflow-auto custom-scroll-bar' style={{height: '100%'}}>
        {
          !eventId &&
          <div className="search-section mb-5">
            <h4>{Utils.translate("label.search.new")}</h4>
            <Input
              value={keyword}
              onChange={(e) => {
                setKeyword(e.target.value);
                setCurrentPageSearch(1);
                setTotalResultSearch(4);
                if (e.target.value !== '') {
                  if (ref.current) {
                    clearTimeout(ref.current);
                  }
                  ref.current = setTimeout(() => {
                    searchBlogs(1, e.target.value).then(_ => {
                    });
                  }, 400)
                }
              }}
              placeholder={Utils.translate("message.tip.search.question")}
              prefix={
                <SearchOutlined
                  className="mr-0"
                />
              }
            />
          </div>
        }
        <div className="recent-post">
          <div className="category">
            <h4 className="mb-2">{Utils.translate("label.recent.post")}</h4>
            <div  className='overflow-auto custom-scroll-bar' style={{height: 400}}>
              {recentBlog.map(blog => (
                <Link href={'/news/' + blog["slug"]} key={blog.slug}>
                  <a>
                    <div className="d-flex mb-3" key={blog["_id"]}>
                      <div>
                        <div className="mr-4" style={{
                          backgroundImage: `url(${blog?.thumbnail ?? "https://data.whicdn.com/images/320132508/original.jpg"})`,
                          width: "90px",
                          height: "60px",
                          backgroundPosition: "center",
                          backgroundSize: "cover",
                          borderRadius: "0.475rem"
                        }}/>
                      </div>
                      <div>
                        <Typography.Paragraph ellipsis={{rows: 3}} className="mb-0 font-weight-semibold"
                                              style={{color: '#455560'}}>{blog?.title}</Typography.Paragraph>
                        <p className="mb-0">{moment(parseInt(blog.createdAt)).format("ll")}</p>
                      </div>
                    </div>
                  </a>
                </Link>
              ))}
            </div>
          </div>
        </div>
      </div>
    </Card>
  )
}
export default BlogSideContent;
