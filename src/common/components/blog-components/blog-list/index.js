import React, {useEffect, useState} from 'react';
import {Avatar, Card, Col, Divider, Empty, Pagination, Row, Spin, Tag} from "antd";
import Link from "next/link";
import {UserOutlined} from "@ant-design/icons";
import moment from "moment";
import Utils from "../../../utils";

const BlogList = (props) => {
  const {
    loading,
    pageInfo,
    blogsData,
    searchBlogData,
    total,
    loadMoreBlog,
    searchBlogs,
    type,
    totalBlogSearch,
    setCurrentPageSearch,
    currentPageSearch,
    totalResultSearch
  } = props;
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    setCurrentPage(1);
  }, [type])

  return (
    <Card bordered={false}>
      <div className="label">
        <div className="mb-3">
          <h4
            className="mb-3">{`${type === 'all' ? Utils.translate("label.all.news") : Utils.translate("label.search.new")} (${type === 'all' ? pageInfo?.totalResults : totalBlogSearch})`}</h4>
          <Divider type="dashed"/>
        </div>
        {(type === 'all' && blogsData?.length > 0) || (type === 'search' && searchBlogData.length > 0) ? (
          <Row gutter="16" className="w-100">
            {(type === 'all' ? blogsData : searchBlogData).map((blog) => {
              return (
                <Col xs={24} sm={24} md={24} lg={12} xl={12} xxl={12} key={blog._id}>
                  <div className="mb-4 p-2">
                    <div className="mb-3">
                      <Link href={`/news/${blog?.slug ?? ''}`}>
                        <a>
                          <div
                            style={{
                              backgroundImage: `url(${blog.thumbnail ? blog.thumbnail : "https://data.whicdn.com/images/320132508/original.jpg"})`,
                              width: "100%",
                              height: "20vh",
                              backgroundPosition: "center",
                              backgroundSize: "cover",
                              borderRadius: "0.475rem"
                            }}
                          >
                          </div>
                        </a>
                      </Link>
                    </div>
                    <Link href={`/news/${blog?.slug ?? ''}`}>
                      <a>
                        <div className="title-post mb-3">
                          <h4>{blog?.title}</h4>
                        </div>
                      </a>
                    </Link>
                    <div className="post-content mb-4 short-description">
                      {Utils.getTextContent(blog?.content)}
                    </div>
                    <div className="post-author d-flex justify-content-between">
                      <div className="d-flex">
                        <Link href={'/profile/' + blog?.author?.username}>
                          <a className='text-gray'>
                            <div className="mr-2">
                              <Avatar shape="circle" src={blog?.author?.avatar} icon={<UserOutlined/>}/>
                            </div>
                          </a>
                        </Link>
                        <Link href={'/profile/' + blog?.author?.username}>
                          <a className='text-gray'>
                            <div className="d-flex flex-column">
                              <span className="m-0">{blog?.author?.displayName}</span>
                              <span
                                className="text-gray m-0">on {moment(parseInt(blog?.createdAt)).format("ll")}</span>
                            </div>
                          </a>
                        </Link>
                      </div>
                      <div className="post-tag">
                        <Tag color="cyan">{Utils.translate("label.tags.exam")}</Tag>
                      </div>
                    </div>
                  </div>
                </Col>
              )
            })}
          </Row>
        ) : (
          <div>
            <Empty description="There are no events yet"/>
          </div>
        )}
      </div>
      {
        !loading
        &&
        <div className='float-right'>
          <Pagination
            current={type === 'all' ? currentPage : currentPageSearch}
            pageSize={4}
            total={type === 'all' ? total : totalResultSearch}
            onChange={(page) => {
              window.scrollTo(top);
              if (type === 'all') {
                setCurrentPage(page);
                loadMoreBlog(page).then(_ => {
                });
              } else {
                setCurrentPageSearch(page);
                searchBlogs(page).then(_ => {
                });
              }
            }}
          />
        </div>
      }
    </Card>
  )
}

export default BlogList;
