import React from "react";
import {Modal} from "antd";

const CustomModal = props => {
  return (
    <Modal
      title={props.title}
      visible={props.visible}
      onCancel={props.onCancel}
      centered={props.centered}
      footer={props.footer}
      width={props.width || 700}
      onOk={props.onOk}
    >
      {props.children}
    </Modal>
  )
}
export default CustomModal;
