import React, {useEffect, useState} from "react";
import {Card, Menu} from "antd";
import Link from "next/link";
import {useRouter} from "next/router";
import {REFRESH_TOKEN} from "../../../configs/AppConfig";
import {onChangeShowPopup} from "../../../redux/actions";
import {useSelector} from "react-redux";
import Utils from "../../../utils";

const QuestionMenuContent = props => {
  const user = useSelector(state => state.user);
  const router = useRouter();
  const selectedPath = router.asPath
  const {locale} = useSelector(state => state.theme)
  return (
    <Card bordered={false} bodyStyle={{
      padding: "0rem 0.8rem",
      borderRight: "1px solid #eff2f5"
    }}>
      <div className="custom-menu">
        <Menu
          defaultSelectedKeys={[selectedPath]}
          defaultOpenKeys={[selectedPath]}
          mode="inline"
          style={{
            borderRadius: ".625rem",
            marginTop: 0
          }}
        >
          <Menu.ItemGroup key="g1" title={Utils.translate("side.menu.community.public")}>
            <Menu.Item key="/">
              <Link href="/">
                <a className="d-flex justify-content-between">
                  <div>{Utils.translate("side.menu.community.all.questions")}</div>
                  <div style={{opacity: .8}}>{`(${props.totalComQuestions})`}</div>
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item key="/forum/search-question">
              <Link href="/forum/search-question">
                <a style={{
                  display: 'block',
                  height: '100%'
                }}>
                  {Utils.translate("side.menu.community.search")}
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item
              style={{fontSize: 13.5, fontWeight: 600}}
              key="/forum/add-question"
              onClick={() => {
                if(user._id && user._id !== '') {
                  return router.push('/forum/add-question').then(_ => {});
                }
                onChangeShowPopup(true);
              }}
            >
              {Utils.translate("side.menu.community.ask.question")}
            </Menu.Item>
          </Menu.ItemGroup>
          <Menu.ItemGroup key="g2" title={Utils.translate("side.menu.community.my.activity")}>
            <Menu.Item
              style={{fontSize: 13.5, fontWeight: 600}}
              key="/forum/my-question"
              onClick={() => {
                if (user._id && user._id !== '') {
                  return router.push('/forum/my-question').then(_ => {
                  });
                }
                onChangeShowPopup(true);
              }}
            >
              {Utils.translate("side.menu.community.my.questions")}
            </Menu.Item>
            <Menu.Item
              style={{fontSize: 13.5, fontWeight: 600}}
              key="/forum/saved-questions"
              onClick={() => {
                if (user._id && user._id !== '') {
                  return router.push('/forum/saved-questions').then(_ => {
                  });
                }
                onChangeShowPopup(true);
              }}>
              {Utils.translate("side.menu.community.saved.questions")}
            </Menu.Item>
          </Menu.ItemGroup>
          <Menu.ItemGroup key="g3" title={Utils.translate("side.menu.community.tag")}>
            <Menu.Item key="/forum/question-tags/exam">
              <Link href="/forum/question-tags/exam">
                <a style={{
                  display: 'block',
                  height: '100%'
                }}>
                  {Utils.translate("side.menu.community.tag.exam")}
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item key="/forum/question-tags/technology">
              <Link href="/forum/question-tags/technology">
                <a style={{
                  display: 'block',
                  height: '100%'
                }}>
                  {Utils.translate("side.menu.community.tag.technologies")}
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item key="/forum/question-tags/learning">
              <Link href="/forum/question-tags/learning">
                <a style={{
                  display: 'block',
                  height: '100%'
                }}>
                  {Utils.translate("side.menu.community.tag.learning")}
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item key="/forum/question-tags/question">
              <Link href="/forum/question-tags/question">
                <a style={{
                  display: 'block',
                  height: '100%'
                }}>
                  {Utils.translate("side.menu.community.tag.question")}
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item key="/forum/question-tags/life_style">
              <Link href="/forum/question-tags/life_style">
                <a style={{
                  display: 'block',
                  height: '100%'
                }}>
                  {Utils.translate("side.menu.community.tag.life.style")}
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item key="/forum/question-tags/other">
              <Link href="/forum/question-tags/other">
                <a style={{
                  display: 'block',
                  height: '100%'
                }}>
                  {Utils.translate("side.menu.community.tag.other")}
                </a>
              </Link>
            </Menu.Item>
          </Menu.ItemGroup>
        </Menu>
      </div>
    </Card>
  )
}
export default QuestionMenuContent;
