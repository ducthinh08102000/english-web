import React from "react";
import {Avatar, Card, Input, Typography} from "antd";
import {RightOutlined, SearchOutlined} from "@ant-design/icons";
import {useRouter} from 'next/router';
import Link from 'next/link';
import Utils from "../../../utils";
import {useSelector} from "react-redux";

const SideContent = props => {
  const { favoriteQuestions, popularQuestions } = props.sideData;
  const {locale} = useSelector(state => state.theme)
  const router = useRouter();
  return (
    <Card bordered={false} bodyStyle={{paddingTop: 0}}>
      <div className={`search-section ${router.pathname !== "/forum/search-question" && 'mb-4'}`}>
        {router.pathname !== "/forum/search-question" && <Input
          placeholder={Utils.translate("label.search.question")}
          prefix={<SearchOutlined className="mr-0"/>}
          onClick={() => {
            router.push('/forum/search-question').then(_ => {
            });
          }}/>}
      </div>
      <div className="p-3 mb-3" style={{
        backgroundColor: "#F5F8FA",
        borderRadius: "8px"
      }}>
        <div className="mb-3">
          <h4>{Utils.translate("label.most.favorite.questions")}</h4>
        </div>
        <div>
          {favoriteQuestions.map((question, index) => (
            <Link key={question._id} href={'/forum/question/' + question?.slug ?? ''}>
              <a>
                <div>
                  <div className="list-popular-questions mb-3">
                    <div className="question-item d-flex">
                      <div className="mr-2">
                        <Avatar icon={<RightOutlined/>} shape="square" size={18}/>
                      </div>
                      <Typography.Paragraph ellipsis={{rows: 2}} className="font-weight-semibold" style={{color: '#455560'}}>
                        {Utils.decodeHtml(question.title)}
                      </Typography.Paragraph>
                    </div>
                  </div>
                </div>
              </a>
            </Link>
          ))}
        </div>
      </div>

      <div className="p-3" style={{
        backgroundColor: "#F5F8FA",
        borderRadius: "8px"
      }}>
        <div className="mb-4">
          <h4>{Utils.translate("label.popular.question")}</h4>
        </div>
        <div>
          {popularQuestions.map((question, index) => (
            <Link key={question._id} href={'/forum/question/' + question?.slug  ?? ''}>
              <a>
                <div>
                  <div className="list-popular-questions mb-3">
                    <div className="question-item d-flex">
                      <div className="mr-2">
                        <Avatar icon={<RightOutlined/>} shape="square" size={18}/>
                      </div>
                      <Typography.Paragraph ellipsis={{rows: 2}} className="font-weight-semibold" style={{color: '#455560'}}>
                        {Utils.decodeHtml(question.title)}
                      </Typography.Paragraph>
                    </div>
                  </div>
                </div>
              </a>
            </Link>
          ))}
        </div>
      </div>
    </Card>
  )
}
export default SideContent;