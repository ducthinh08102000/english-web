import React from "react";
import QuestionList from "common/components/forum-components/question-list";
import {Button, Card, Divider, Input} from "antd";

const SearchQuestion = props => {
  return (
    <div>
      <Card bordered={false}>
        <div className="header-content">
          <div className="d-flex mb-4">
            <Input className="mr-2" placeholder="Enter keyword..."/>
            <Button type="primary">Search</Button>
          </div>
          <div className="counter-result">
            <p>56 results found for keyword “Laravel”</p>
          </div>
          <Divider type="dashed" />
        </div>
        <QuestionList {...props} />
      </Card>
    </div>
  )
}
export default SearchQuestion;
