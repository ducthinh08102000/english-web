import React from "react";
import {Button, Col, Radio, Row, Tooltip} from "antd";
import Link from "next/link";
import {EditOutlined} from "@ant-design/icons";
import Utils from "../../../utils";
import {ROW_GUTTER} from "../../../constants/ThemeConstant";

const SampleQuestionCard = ({question, index = 0}) => {
  return (
    <div>
      <div className="mb-1">
        <b>Question {question?.id || 1} </b>
        <Tooltip title={"Edit question" + (question?.id)}>
          <Link href={"/manage/question/" + question?.id + "/edit"} passHref>
            <Button icon={<EditOutlined/>} size="small" type="link" ghost></Button>
          </Link>
        </Tooltip>
      </div>
      <div className="mb-2" dangerouslySetInnerHTML={{__html: `${question?.data?.question}`}}/>
      <div>
        <Radio.Group value={Utils.convertAnswerToNumber(question?.data?.correct)}>
          <Row gutter={ROW_GUTTER}>
            {question.data.answers.map((ans, i) => (
              <Col xs={24} sm={24} md={12} lg={6} xl={6} key={index}>
                <Radio value={i}>
                  <div dangerouslySetInnerHTML={{
                    __html: `${ans}`
                  }}/>
                </Radio>
              </Col>
            ))}
          </Row>
        </Radio.Group>
      </div>
    </div>
  )
}
export default SampleQuestionCard
