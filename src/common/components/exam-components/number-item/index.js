import React from "react";

const NumberItem = ({state, number}) => {
  const style = {
    boxStyleDefault: {
      backgroundColor: "#3e79f7",
      height: "40px",
      width: "40px",
      display: "flex",
      borderRadius: "25%",
      border: "0"
    },
    numberStyle: {
      color: "white",
      margin: "auto",
    }
  }
  const changeColorByStatus = (status) => {
    switch (status) {
      case "wrong":
        return {
          ...style.boxStyleDefault,
          backgroundColor: "#ff6b72",
        }
      case "flag":
        return {
          ...style.boxStyleDefault,
          backgroundColor: "#ffc542",
        }
      case "done":
        return {
          ...style.boxStyleDefault,
          backgroundColor: "#f67a27",
        }
      case "correct":
        return {
          ...style.boxStyleDefault,
          backgroundColor: "rgb(4, 209, 130)",
        }
      default:
        return {
          ...style.boxStyleDefault,
        }
    }
  }

  return (
    <div className="number mr-2 cursor-pointer" style={changeColorByStatus(state)}>
      <p style={style.numberStyle}>{number}</p>
    </div>
  )
}
export default NumberItem;
