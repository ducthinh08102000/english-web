import useSWR from "swr";
import sendRequest from "../services/RequestService";
import axios from "axios";

const fetchPublicData = (url, params) => axios.get(url, {params}).then(res =>  ({data: res.data, status: res.status}));

export default function useSWRCustom(key, method, option = {}, shouldFetch = true, isPublic) {
  const {data, mutate, error} = useSWR(shouldFetch ? [key, method, option] : null, isPublic ? url => fetchPublicData(url, option) : sendRequest)
  const loading = !data && !error;
  return {
    loading,
    data,
    error,
    mutate
  }
}
