import useSWRCustom from "./useSWRCustom";

export const useUser = (options, shouldFetch = true) => {
  return useSWRCustom("/api/users", "get", options, shouldFetch)
}

export const useExam = (options, shouldFetch = true) => {
  return useSWRCustom("/api/exams", "get", options, shouldFetch, 'public')
}

export const useQuestion = (options, shouldFetch = true) => {
  return useSWRCustom("/api/questions", "get", options, shouldFetch)
}

export const useBlogs = (options, shouldFetch = true) => {
  return useSWRCustom("/api/blogs", "get", options, shouldFetch)
}

export const useRanking = (examCode, options, shouldFetch = true) => {
  return useSWRCustom("/api/exams/ranking/" + examCode, "get", options, shouldFetch)
}


export const useIssues = (options, shouldFetch = true) => {
  return useSWRCustom("/api/issues", "get", options, shouldFetch)
}
