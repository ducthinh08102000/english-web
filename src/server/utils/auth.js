import httpStatus from "http-status";
import connectDb from "server/utils/connect-db";
import ApiError from "server/utils/api-error";
import {verifyTokenFromCookies} from "server/utils/verify-token";
/**
 * @param {Object} context
 * @param {Array} permissions
 */
const auth = async (context, permissions = null) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  await connectDb();
  const {req, res} = context;
  let {access_token, refresh_token} = context.req.cookies;

  if (!refresh_token) {
    throw new ApiError(httpStatus.UNAUTHORIZED, "Authorization Failed");
  }
  let {user} = await verifyTokenFromCookies(req, res, access_token, refresh_token);

  if (permissions && permissions.length > 0) {
    const userPermission = user.role.permissions;
    const hasRequiredPermission = permissions.some(permission => userPermission.includes(permission));
    if (!hasRequiredPermission) {
      throw new ApiError(httpStatus.UNAUTHORIZED, "Access is not authorized.");
    }
  }
  return {user};
}

module.exports = auth;
