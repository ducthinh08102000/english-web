import {errorHandler} from "server/middleware"

const catchAsync = (fn) => (req, res) => {
  Promise.resolve(fn(req, res)).catch((err) => {
    errorHandler(err, req, res);
  });
};

module.exports = catchAsync;
