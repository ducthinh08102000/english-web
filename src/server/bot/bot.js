import axios from "axios";

export class Bot {
  static accessToken = "";

  constructor() {

  }

  static startup() {

  }

  static verify(req, res, next) {
    const verifyToken = process.env.VERIFY_TOKEN;
    let mode = req.query['hub.mode'];
    let token = req.query['hub.verify_token'];
    let challenge = req.query['hub.challenge'];
    if (mode && token === verifyToken) {
      res.send(challenge);
    } else {
      res.status(403);
    }
  }

  static setCredentials(accessToken) {
    this.accessToken = accessToken;
  }

  static send(message) {
    return new Promise((resolve, reject) => {
      axios({
        method: "POST",
        url: `https://graph.facebook.com/v12.0/me/messages?access_token=${this.accessToken}`,
        headers: {
          "Content-Type": "application/json"
        },
        data: message
      }).then(resolve).catch(err => {
        console.log(err);
        reject();
      });
    });
  }

  static getUserInfo(userId) {
    return new Promise((resolve, reject) => {
      axios({
        method: "GET",
        url: `https://graph.facebook.com/v12.0/${userId}`,
        headers: {
          "Content-Type": "application/json"
        },
        params: {
          fields: `first_name,last_name,profile_pic,id,name`,
          access_token: this.accessToken
        }
      }).then(response => {
        resolve(response.data);
      }).catch(err => {
        console.log(err);
        reject();
      });
    });
  }

  static setMessengerProfile(profile) {
    return new Promise((resolve, reject) => {
      axios({
        method: "POST",
        url: `https://graph.facebook.com/v12.0/me/messenger_profile?access_token=${this.accessToken}`,
        headers: {
          "Content-Type": "application/json"
        },
        data: profile
      }).then(resolve).catch(err => {
        console.log(err);
        reject();
      });
    });
  }
}
