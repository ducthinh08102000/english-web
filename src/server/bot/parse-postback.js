const {Bot} = require("server/bot/bot");
const Message = require("./lib/message");
const Template = require("./lib/template");
const TemplateElement = require("./lib/template-element");
const Button = require("./lib/button");
const QuickRepliesButton = require("./lib/quick-replies-button");
const parsePayload = require("./parse-payload");

const parsePostback = async (messagingEntry, user) => {
  const sender = messagingEntry['sender'];
  const senderId = sender['id'];
  const msg = messagingEntry['message'];
  const postbackData = messagingEntry['postback'];
  await parsePayload(postbackData.payload, senderId, user);
};

module.exports = parsePostback;
