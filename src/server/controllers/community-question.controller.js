import {communityQuestionService, userActivityService} from '../services';
import pick from "server/utils/pick";

export const addQuestion = async (req, res) => {
  Object.assign(req.body, {author: req.user._id});
  const question = await communityQuestionService.createQuestion(req.body);
  res.json({
    message: "Created Question successfully",
    question
  });
  await userActivityService.createUserActivity({
    message: `has added a new forum question`,
    detail: question.title,
    target: question._id,
    model: "CommunityQuestion",
    user: req.user._id
  });
};

export const getQuestions = async (req, res) => {
  const filter = pick(req.query, ['title', 'slug', "author", "tag"]);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  Object.assign(filter, {deleted: {$ne: true}});
  let userId = req.user ? req.user._id : null;
  const result = await communityQuestionService.queryQuestions(filter, options, userId);
  res.json(result);
};

export const getSavedQuestion = async (req, res) => {
  const filter = {saved: true, deleted: {$ne: true}};
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  let userId = req.user ? req.user._id : null;
  const result = await communityQuestionService.queryQuestions(filter, options, userId);
  res.json(result);
};

export const getDeletedQuestions = async (req, res) => {
  const filter = pick(req.query, ['title', 'slug']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  Object.assign(filter, {deleted: true});
  const result = await communityQuestionService.queryQuestions(filter, options);
  res.json(result);
};

export const getQuestion = async (req, res) => {
  console.log(req.user)
  let userId = req.user ? req.user._id : null;
  const result = await communityQuestionService.getQuestionByFilter({slug: req.query.slug}, userId);
  res.json(result);
};

export const updateQuestion = async (req, res) => {
  const question = await communityQuestionService.updateQuestion(req.query.slug, req.body, req.user.role.permissions.includes('MANAGE_ALL_COMMUNITY_QUESTION') || req.user.role.permissions.includes('UPDATE_ALL_COMMUNITY_QUESTION'), req.user._id);
  res.json({
    message: "Updated question successfully",
    question
  });
  await userActivityService.createUserActivity({
    message: `has updated a forum question`,
    detail: question.title,
    target: question._id,
    model: "CommunityQuestion",
    user: req.user._id
  });
};

export const saveQuestion = async (req, res) => {
  const {question, message} = await communityQuestionService.saveQuestion(req.query.slug, req.user._id);
  res.json({
    message: message,
    question
  });
  await userActivityService.createUserActivity({
    message: (message.includes("Unsaved")) ? `has unsaved a forum question` : `has saved a forum question`,
    detail: question.title,
    target: question._id,
    model: "CommunityQuestion",
    user: req.user._id
  });
};

export const deleteQuestion = async (req, res) => {
  const question = await communityQuestionService.deleteQuestion(req.query.slug, req.user.role.permissions.includes('MANAGE_ALL_COMMUNITY_QUESTION') || req.user.role.permissions.includes('DELETE_ALL_COMMUNITY_QUESTION'), req.user._id);
  res.json({
    message: "Deleted question successfully",
    question
  });
  await userActivityService.createUserActivity({
    message: `has deleted a forum question`,
    detail: question.title,
    target: question._id,
    model: "CommunityQuestion",
    user: req.user._id
  });
};

export const restoreQuestions = async (req, res) => {
  const result = await communityQuestionService.restoreQuestions(req.body);
  res.json({
    message: "Restore Questions successfully",
    result
  });
};

export const deleteQuestionsPermanently = async (req, res) => {
  const result = await communityQuestionService.deleteQuestionsPermanently(req.body);
  res.json({
    message: "Delete questions successfully",
    result
  });
};