import {issueService, userActivityService} from '../services';
import pick from "../utils/pick";

export const addIssue = async (req, res) => {
  const issue = await issueService.createIssue(req.body, req.user._id);
  res.json({
    message: "Created Issue successfully",
    issue
  });
  await userActivityService.createUserActivity({
    message: `has added a new issue`,
    detail: issue.title,
    target: issue._id,
    model: "Issue",
    user: req.user._id
  });
};

export const getIssues = async (req, res) => {
  const filter = pick(req.query, ['user', 'type', 'state', 'slug', 'title', 'model']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  Object.assign(filter, {deleted: {$ne: true}});
  const result = await issueService.queryIssues(filter, options, req.user.role.permissions.includes('MANAGE_ALL_ISSUE'), req.user._id);
  res.json(result);
};

export const getIssue = async (req, res) => {
  const result = await issueService.getIssueByFilter({_id: req.query.issueId}, req.user.role.permissions.includes('MANAGE_ALL_ISSUE'), req.user._id);
  res.json(result);
};

export const updateIssue = async (req, res) => {
  const issue = await issueService.updateIssue(req.query.issueId, req.body, req.user.role.permissions.includes('MANAGE_ALL_ISSUE'), req.user._id);
  res.json({
    message: "Updated Issue successfully",
    issue
  });
  await userActivityService.createUserActivity({
    message: `has updated a issue`,
    detail: issue.title,
    target: issue._id,
    model: "Issue",
    user: req.user._id
  });
};

export const deleteIssue = async (req, res) => {
  const issue = await issueService.deleteIssue(req.query.issueId, req.user.role.permissions.includes('MANAGE_ALL_ISSUE'), req.user._id);
  res.json({
    message: "Deleted Issue successfully",
    issue
  });
  await userActivityService.createUserActivity({
    message: `has deleted a new issue`,
    detail: issue.title,
    target: issue._id,
    model: "Issue",
    user: req.user._id
  });
};

export const getDeletedIssues = async (req, res) => {
  const filter = pick(req.query, ['user', 'type', 'state', 'slug', 'title']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  Object.assign(filter, {deleted: true});
  const result = await issueService.queryIssues(filter, options, req.user.role.permissions.includes('MANAGE_ALL_ISSUE'), req.user._id);
  res.json(result);
};

export const restoreIssues = async (req, res) => {
  const result = await issueService.restoreIssues(req.body);
  res.json({
    message: "Restore Issues successfully",
    result
  });
};

export const deleteIssuesPermanently = async (req, res) => {
  const result = await issueService.deleteIssuesPermanently(req.body);
  res.json({
    message: "Delete issues successfully",
    result
  });
};