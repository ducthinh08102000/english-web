import {errorHandler} from "server/middleware";
import {userActivityService} from "server/services"

const recordUserActivity = (req, res, activityBody) => {
  return new Promise(async (resolve) => {
    try {
      await userActivityService.createUserActivity(activityBody);
      resolve();
    } catch (err) {
      errorHandler(err, req, res);
    }
  });
}

export default recordUserActivity;