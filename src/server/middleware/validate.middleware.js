import Joi from 'joi';
import httpStatus from 'http-status';
import pick from 'server/utils/pick';
import ApiError from 'server/utils/api-error';
import {errorHandler} from "server/middleware"

const validateMiddleware = (req, res, schema) => {
  return new Promise(resolve => {
    try {
      const validSchema = pick(schema, ['params', 'query', 'body']);
      const object = pick(req, Object.keys(validSchema));
      const {value, error} = Joi.compile(validSchema)
        .prefs({errors: {label: 'key'}, abortEarly: false})
        .validate(object);

      if (error) {
        const errorMessage = error.details.map((details) => details.message).join(', ');
        throw new ApiError(httpStatus.BAD_REQUEST, errorMessage);
      }
      Object.assign(req, value);
      resolve();
    } catch (err) {
      errorHandler(err, req, res);
    }
  });
};

export default validateMiddleware;
