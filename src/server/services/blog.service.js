import httpStatus from 'http-status';
import {Blog} from '../models';
import ApiError from '../utils/api-error';
import logger from '../config/logger.config';
import mongoose from "mongoose";
import {deleteFile} from "server/utils/google-storage";
import {gCloud} from "server/config";

/**
 * Create a Blog
 * @param {Object} blogBody
 * @param {ObjectId} authorId
 * @returns {Promise<blog>}
 */
export const createBlog = async (blogBody, authorId) => {
  Object.assign(blogBody, {author: authorId});
  const blog = await Blog.create(blogBody);
  await blog.populate({path: "author", select: "username displayName avatar"});
  return blog;
};

/**
 * Get news by filter
 * @param {Object} filter
 * @param {Object} options
 * @param {ObjectId} userId
 * @returns {Object}
 */
export const getBlogsByFilter = async (filter, options, userId = null) => {
  const objectIdFields = ["author"];
  const excludeFields = ["_id", "deleted"];

  for (let field in filter) {
    if (objectIdFields.includes(field) || (filter._id && typeof filter._id !== "object")) filter[field] = mongoose.Types.ObjectId(filter[field]);
    else if (excludeFields.includes(field)) continue
    else filter[field] = {$regex: filter[field], $options: 'g'}
  }

  let blogs = await Blog.aggregate([
    {
      $match: {
        ...filter
      }
    },
    {
      $lookup: {
        from: 'users',
        localField: 'author',
        foreignField: '_id',
        as: 'author',
      }
    },
    {$unwind: "$author"},
    {
      $lookup: {
        from: 'comments',
        localField: '_id',
        foreignField: 'category',
        as: 'comments',
      }
    },
    {
      $lookup: {
        from: 'reactions',
        localField: '_id',
        foreignField: 'target',
        as: 'reactions',
      }
    },
    {
      $project: {
        "author._id": 1,
        "author.username": 1,
        "author.email": 1,
        "author.fullName": 1,
        "author.displayName": 1,
        "author.avatar": 1,
        "title": 1,
        "slug": 1,
        "categories": 1,
        "content": 1,
        "thumbnail": 1,
        "reactions": 1,
        "createdAt": 1,
        "editedAt": 1,
        "editedTimes": 1,
        "reactionCount": {$size: "$reactions"},
        "commentCount": {$size: "$comments"}
      }
    },
    {
      $sort: options.sort || {createdAt: -1}
    },
    {
      $skip: options.skip || 0
    },
    {
      $limit: options.limit || 10
    },
  ]);
  for (let blog of blogs) {
    blog.createdAt = new Date(blog.createdAt).getTime();
    let reactions = [...blog.reactions];
    blog.reactions = reactions.reduce((previousValue, currentValue) => {
      return previousValue[currentValue.type] ? ++previousValue[currentValue.type] : previousValue[currentValue.type] = 1, previousValue;
    }, {})
    let userReaction = userId ? reactions.find(r => r.user.toString() === userId.toString()) : null;
    blog.userReaction = userReaction ? userReaction.type : false;
  }
  return blogs;
};

/**
 * Query for Blogs
 * @param {ObjectId} userId
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @param {object} [options.sort] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
export const queryBlogs = async (filter, options, userId = null) => {
  options.sort = {};
  options.limit = options.limit && parseInt(options.limit, 10) > 0 ? parseInt(options.limit, 10) : 9;
  options.page = options.page && parseInt(options.page, 10) > 0 ? parseInt(options.page, 10) : 1;
  if (options.sortBy) {
    if (options.sortBy.includes("-")) options.sort[`${options.sortBy.split("-")[1]}`] = -1;
    else options.sort[`${options.sortBy}`] = 1;
  }
  if (Object.keys(options.sort).length < 1) options.sort = null;
  Object.assign(options, {skip: (options.page - 1) * options.limit});

  return Promise.all([getBlogsByFilter(filter, options, userId), Blog.count({...filter})]).then((values) => {
    let [questions, totalResults] = values;
    const totalPages = Math.ceil(totalResults / options.limit);
    const result = {
      results: questions,
      page: options.page,
      limit: options.limit,
      totalPages: totalPages,
      totalResults: totalResults,
    };
    return Promise.resolve(result);
  })
};

/**
 * Get blogs by filter
 * @param {Object} filter
 * @param {ObjectId} userId
 * @returns {Promise<blog>}
 */
export const getBlogByFilter = async (filter, userId = null) => {
  const blogs = await getBlogsByFilter({...filter, deleted: {$ne: true}}, {limit: 1}, userId);
  if (!blogs || blogs.length < 1) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Blog not found');
  }
  return blogs[0];
};

/**
 * Update blogs by filter
 * @param {String} slug
 * @param {Object} updateBody
 * @param {Boolean} hasPermission
 * @param {ObjectId} userId
 * @param {File} file
 * @returns {Promise<blog>}
 */
export const updateBlog = async (slug, updateBody, hasPermission, userId, file = null) => {
  const blog = await Blog.findOne({slug});
  if (!blog || blog.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Blog not found');
  }
  if (!hasPermission && blog.author._id.toString() !== userId.toString()) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }
  if (file) {
    updateBody.thumbnail = file.linkUrl;
    try {
      if (blog.thumbnail && blog.thumbnail.startsWith("https://storage.googleapis.com/")) await deleteFile(blog.thumbnail);
    } catch (e) {
      logger.error("Failed to delete thumbnail");
    }
  }
  Object.assign(blog, updateBody);
  Object.assign(blog, {editedAt: Date.now(), editedTimes: blog.editedTimes ? ++blog.editedTimes : 1});
  await blog.save();
  return blog;
};

/**
 * Delete blogs
 * @param {String} slug
 * @param {Boolean} hasPermission
 * @param {ObjectId} userId
 * @return {Promise<blog>}
 */
export const deleteBlog = async (slug, hasPermission, userId) => {
  const blog = await Blog.findOne({slug});
  if (!blog || blog.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Blog not found');
  }
  if (!hasPermission && blog.author._id.toString() !== userId.toString()) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }
  Object.assign(blog, {deleted: true});
  await blog.save();
  return blog;
}

/**
 * Restore blogs
 * @param {Object} body
 * @returns {Promise}
 */
export const restoreBlogs = async (body) => {
  if (!body.blogs || body.blogs.length <= 0) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Bad request');
  }
  let blogs = [...body.blogs];
  let execute = blogs.map(r => {
    return {
      updateOne: {
        filter: {
          _id: r,
        },
        update: {
          deleted: false,
        }
      }
    }
  })
  return Blog.bulkWrite(execute);
}

/**
 * Delete Blogs Permanently
 * @param {Object} body
 * @returns {Promise}
 */
export const deleteBlogsPermanently = async (body) => {
  if (!body.blogs || body.blogs.length <= 0) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Bad request');
  }
  let blogs = await Blog.find({_id: {$in: body.blogs}});

  if (blogs.length === 0) throw new ApiError(httpStatus.NOT_FOUND, "No news found");

  for (const blog of blogs) {
    if (blog.thumbnail && blog.thumbnail.startsWith(gCloud.baseUrl)) await deleteFile(blog.thumbnail);
    await blog.deleteOne({});
  }
}
