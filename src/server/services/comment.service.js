import httpStatus from 'http-status';
import {Comment, Reaction} from '../models';
import ApiError from '../utils/api-error';
import mongoose from "mongoose";
import {userActivityService} from "./index";

/**
 * Create a Comment
 * @param {Object} commentBody
 * @param {ObjectId} authorId
 * @returns {Promise<Comment>}
 */
export const createComment = async (commentBody, authorId) => {
  Object.assign(commentBody, {author: authorId});
  const comment = await Comment.create(commentBody);
  await comment.populate([
    {path: "author", select: "username displayName avatar"},
    {path: "category", model: commentBody.model}
  ]);
  return comment;
};

/**
 * Get comments by filter
 * @param {Object} filter
 * @param {Object} options
 * @param {ObjectId} userId
 * @returns {Promise<question>}
 */
export const getCommentsByFilter = async (filter, options, userId = null) => {
  if (filter._id) filter._id = mongoose.Types.ObjectId(filter._id);
  if (filter.replyFor) filter.replyFor = mongoose.Types.ObjectId(filter.replyFor);
  if (filter.category) filter.category = mongoose.Types.ObjectId(filter.category);

  let comments = await Comment.aggregate([
    {
      $match: {
        ...filter
      }
    },
    {
      $lookup: {
        from: 'users',
        localField: 'author',
        foreignField: '_id',
        as: 'author',
      }
    },
    {$unwind: "$author"},
    {
      $lookup: {
        from: 'comments',
        localField: '_id',
        foreignField: 'replyFor',
        as: 'subComments',
      }
    },
    {
      $lookup: {
        from: 'reactions',
        localField: '_id',
        foreignField: 'target',
        as: 'reactions',
      }
    },
    {
      $project: {
        "author._id": 1,
        "author.username": 1,
        "author.email": 1,
        "author.fullName": 1,
        "author.displayName": 1,
        "author.avatar": 1,
        "_id": 1,
        "content": 1,
        "replyFor": 1,
        "level": 1,
        "category": 1,
        "editedAt": 1,
        "createdAt": 1,
        "editedTimes": 1,
        "subCommentCount": {$size: "$subComments"},
        "reactions": 1,
        "reactionCount": {$size: "$reactions"},
      }
    },
    {
      $sort: options.sort || {createdAt: -1}
    },
    {
      $skip: options.skip || 0
    },
    {
      $limit: options.limit || 10
    },
  ]);
  for (let data of comments) {
    data.createdAt = new Date(data.createdAt).getTime();
    let reactions = [...data.reactions];
    data.reactions = reactions.reduce((previousValue, currentValue) => {
      return previousValue[currentValue.type] ? ++previousValue[currentValue.type] : previousValue[currentValue.type] = 1, previousValue;
    },{})
    let userReaction = userId ? reactions.find(r => r.user.toString() === userId.toString()) : null;
    data.userReaction = userReaction ? userReaction.type : false;
  }
  return comments;
};

/**
 * Query for Comments
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {ObjectId} userId
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {object} [options.sort] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise}
 */
export const queryComments = async (filter, options, userId) => {
  options.sort = {};
  options.limit = options.limit && parseInt(options.limit, 10) > 0 ? parseInt(options.limit, 10) : 9;
  options.page = options.page && parseInt(options.page, 10) > 0 ? parseInt(options.page, 10) : 1;
  if (options.sortBy) {
    if (options.sortBy.includes("-")) options.sort[`${options.sortBy.split("-")[1]}`] = -1;
    else options.sort[`${options.sortBy}`] = 1;
  }
  if (Object.keys(options.sort).length < 1) options.sort = null;
  Object.assign(options, {skip: (options.page - 1) * options.limit});

  return Promise.all([getCommentsByFilter(filter, options, userId), Comment.count({...filter})]).then((values) => {
    let [questions, totalResults] = values;
    const totalPages = Math.ceil(totalResults / options.limit);
    const result = {
      results: questions,
      page: options.page,
      limit: options.limit,
      totalPages: totalPages,
      totalResults: totalResults,
    };
    return Promise.resolve(result);
  })
};

/**
 * Get Comment by filter
 * @param {Object} filter
 * @param {ObjectId} userId
 * @returns {Promise<Comment>}
 */
export const getCommentByFilter = async (filter, userId = null) => {
  const comment = await getCommentsByFilter(filter, {skip: 0, limit: 1}, userId);
  if (!comment) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Comment not found');
  }
  return comment[0];
};

/**
 * Update Comment by filter
 * @param {ObjectId} commentId
 * @param {Object} updateBody
 * @param {Boolean} hasPermission
 * @returns {Promise<Comment>}
 */
export const updateComment = async (commentId, updateBody, hasPermission) => {
  const {author} = updateBody;
  const comment = await Comment.findOne({_id: commentId});
  if (!comment || comment.deleted) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Comment not found');
  }
  if (!hasPermission && comment.author._id.toString() !== author.toString()) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }
  Object.assign(comment, updateBody);
  Object.assign(comment, {editedAt: Date.now(), editedTimes: comment.editedTimes ? ++comment.editedTimes : 1});
  await comment.save();
  await comment.populate([
    {path: "author", select: "username displayName avatar"},
    {path: "category", model: updateBody.model}
  ]);
  return comment;
};

/**
 * Delete Comment
 * @param {Object} deleteData
 * @param {Boolean} hasPermission
 * @return {Promise<Comment>}
 */
export const deleteComment = async (deleteData, hasPermission) => {
  const {commentId, author} = deleteData;
  const comment = await Comment.findOne({_id: commentId});
  if (!comment) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Comment not found');
  }
  if (!hasPermission && comment.author._id.toString() !== author.toString()) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }

  await comment.deleteOne({});
  return comment;
}