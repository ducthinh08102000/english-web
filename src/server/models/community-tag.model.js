import mongoose from 'mongoose';
import slugify from "../utils/slugify";
import {toJSON, paginate} from './plugins';
import CommunityQuestionTag from './community-question-tag.model';

const comTagSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  slug: {
    type: String,
  },
}, {
  collection: "communitytags",
  timestamps: true,
});

// add plugin that converts mongoose to json
comTagSchema.plugin(toJSON);
comTagSchema.plugin(paginate);


/**
 * Slug generator
 * @param {string} tagTitle - The tag's title
 * @returns {Promise<string>}
 */
comTagSchema.statics.slugGenerator = async function(tagTitle) {
  let newSlug = slugify(tagTitle);
  let count = 0;
  while (await this.exists({slug: newSlug})) {
    newSlug = `${slugify(tagTitle)}_${++count}`;
  }
  return newSlug;
};

comTagSchema.pre('save', async function (next) {
  const tag = this;
  if (tag.isModified("title")) {
    tag.slug = await CommunityTag.slugGenerator(tag.title);
  }
  next();
});

comTagSchema.pre("deleteOne", { document: true, query: false }, async function (next) {
  const tag = this;
  await CommunityQuestionTag.deleteMany({tag: tag._id});
  next();
});


/**
 * @typedef CommunityTag
 */
const CommunityTag = mongoose.models.CommunityTag || mongoose.model('CommunityTag', comTagSchema);

export default CommunityTag;
