import mongoose from 'mongoose';
import {paginate, toJSON} from "./plugins";

const uComQuestionSchema = new mongoose.Schema({
  question: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
}, {
  collection: 'usercomquestions',
  timestamps: true
});

uComQuestionSchema.plugin(paginate);
uComQuestionSchema.plugin(toJSON);

/**
 * @typedef UserActivity
 */
const UserComQuestion = mongoose.models.UserComQuestion || mongoose.model("UserComQuestion", uComQuestionSchema);

export default UserComQuestion;