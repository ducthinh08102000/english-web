import mongoose from 'mongoose';
import {toJSON, paginate} from './plugins';
import {state, type} from '../config/issue.config';
import slugify from "../utils/slugify";

const issueSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  target: {
    type: mongoose.Schema.Types.ObjectId,
  },
  title: {
    type: String,
    required: true,
  },
  slug: {
    type: String,
  },
  content: {
    type: String,
    required: true,
  },
  state: {
    type: String,
    enum: state.values,
    default: state.default,
  },
  model: {
    type: String
  },
  type: {
    type: String,
    enum: type.values,
    default: type.default,
  },
  deleted: {
    type: Boolean,
    default: false,
    private: true
  },
}, {
  collection: "issues",
  timestamps: true,
});

// add plugin that converts mongoose to json
issueSchema.plugin(toJSON);
issueSchema.plugin(paginate);

/**
 * Slug generator
 * @param {string} issueTitle - The question's title
 * @returns {Promise<string>}
 */
issueSchema.statics.slugGenerator = async function(issueTitle) {
  let newSlug = slugify(issueTitle);
  let count = 0;
  while (await this.exists({slug: newSlug})) {
    newSlug = `${slugify(issueTitle)}_${++count}`;
  }
  return newSlug;
};

issueSchema.pre('save', async function (next) {
  const issue = this;
  if (issue.isModified("title")) {
    issue.slug = await Issue.slugGenerator(issue.title);
  }
  next();
});

/**
 * @typedef Issue
 */
const Issue = mongoose.models.Issue || mongoose.model('Issue', issueSchema);

export default Issue;
