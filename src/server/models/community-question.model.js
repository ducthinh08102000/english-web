import mongoose from 'mongoose';
import {toJSON, paginate} from './plugins';
import {privacy} from '../config/community-question.config';
import slugify from "../utils/slugify";
import Comment from "./comment.model";
import UserActivity from "./user-activity.model";
import Reaction from "./reaction.model";

const comQuestionSchema = new mongoose.Schema({
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  title: {
    type: String,
    required: true,
  },
  slug: {
    type: String,
  },
  content: {
    type: String,
    required: true,
  },
  // resolvedAnswer: {
  //   type: mongoose.Schema.Types.ObjectId,
  //   ref: "Comment",
  // },
  privacy: {
    type: String,
    enum: privacy.values,
    default: privacy.default,
  },
  editedAt: {
    type: String,
  },
  editedTimes: {
    type: Number,
  },
  deleted: {
    type: Boolean,
    default: false,
    private: true
  },
}, {
  collection: "communityquestions",
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true},
});

// add plugin that converts mongoose to json
comQuestionSchema.plugin(toJSON);
comQuestionSchema.plugin(paginate);

comQuestionSchema.virtual("comments", {
  ref: "Comment",
  localField: "_id",
  foreignField: "category"
});

/**
 * Slug generator
 * @param {string} questionTitle - The question's title
 * @returns {Promise<string>}
 */
comQuestionSchema.statics.slugGenerator = async function(questionTitle) {
  let newSlug = slugify(questionTitle);
  let count = 0;
  while (await this.exists({slug: newSlug})) {
    newSlug = `${slugify(questionTitle)}_${++count}`;
  }
  return newSlug;
};

comQuestionSchema.pre('save', async function (next) {
  const question = this;
  if (question.isModified("title")) {
    question.slug = await CommunityQuestion.slugGenerator(question.title);
  }
  next();
});

// will call before remove method => cascading delete all blogs model references
comQuestionSchema.pre("deleteOne", {document: true, query: false}, async function (next) {
  const question = this;
    await Comment.deleteMany({category: question._id});
    await UserActivity.deleteMany({target: question._id});
    await Reaction.deleteMany({target: question._id});
  next();
});

/**
 * @typedef CommunityQuestion
 */
const CommunityQuestion = mongoose.models.CommunityQuestion || mongoose.model('CommunityQuestion', comQuestionSchema);

export default CommunityQuestion;
