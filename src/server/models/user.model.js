import mongoose from 'mongoose';
import validator from 'validator';
import bcrypt from 'bcryptjs';
import {toJSON, paginate} from './plugins';
import Role from "./role.model";
import httpStatus from "http-status";
import ApiError from "../utils/api-error";
import defaultAvatar from "server/config/upload.config"

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
  },
  id: {
    type: Number
  },
  email: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error('Invalid email');
      }
    },
  },
  password: {
    type: String,
    required: true,
    trim: true,
    minlength: 8,
    validate(value) {
      if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
        throw new Error('Password must contain at least one letter and one number');
      }
    },
    private: true, // used by the toJSON plugin
  },
  role: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Role"
  },
  dob: {
    type: String,
  },
  gender: {
    type: String,
  },
  fullName: {
    type: String,
  },
  displayName: {
    type: String,
  },
  avatar: {
    type: String,
    default: defaultAvatar.defaultData.path
  },
  bio: {
    type: String
  },
  facebookInfo: {
    type: {
      facebookId: String,
      name: String,
      avatar: String
    },
    default: null
  },
  googleInfo: {
    type: {
      googleId: String,
      name: String,
      avatar: String
    },
    default: null
  },
  deleted: {
    type: Boolean,
    default: false,
    private: true
  },
  isVerifiedEmail: {
    type: Boolean,
    default: false
  },
  privateFields: {
    type: [],
    private: true
  }
}, {
  collection: "users",
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true},
});

userSchema.virtual("activities", {
  ref: "UserActivity",
  localField: "_id",
  foreignField: "user"
});

// add plugin that converts mongoose to json
userSchema.plugin(toJSON);
userSchema.plugin(paginate);

/**
 * Check if email is taken
 * @param {Object} filter - Unique field
 * @param {ObjectId} [excludeUserId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */
userSchema.statics.isFieldTaken = async function (filter, excludeUserId) {
  const user = await this.findOne({...filter, _id: {$ne: excludeUserId}});
  return !!user;
};

/**
 * Handle role (covert from name to ObjectId and check if exists)
 * @param {string} data - role field
 * @returns {Promise<boolean>}
 */
userSchema.statics.handleRole = async function (data) {
  const {_id: role} = await Role.findOne((mongoose.isValidObjectId(data) ? {_id: data} : {$or: [{slug: data}, {name: data}]}));
  if (!role) throw new ApiError(httpStatus.NOT_FOUND, "Role not found");
  return role;
};

/**
 * Check if password matches the user's password
 * @param {string} password
 * @returns {Promise<boolean>}
 */
userSchema.methods.isPasswordMatch = async function (password) {
  const user = this;
  return bcrypt.compare(password, user.password);
};

userSchema.pre('save', async function (next) {
  const user = this;
  if (user.isModified('password')) {

  }
  if (!user.id) {
    const checkUser = await User.findOne({}).sort({id: -1});
    let padding = Math.floor(Math.random() * 6) + 1;
    let defaultID = 10000000;
    user.id = (checkUser ? (checkUser.id || defaultID) : defaultID) + padding;
  }

  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }

  if (!user.fullName) {
    user.fullName = this.username;
  }

  if (!user.displayName) {
    user.displayName = this.username;
  }

  if (!user.role) {
    user.role = (await Role.findOne({name: "user"}).lean())._id;
  }
  next();
});

/**
 * @typedef User
 */
const User = mongoose.models.User || mongoose.model('User', userSchema);

export default User;
