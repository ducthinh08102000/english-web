import Joi from 'joi';
import {password} from 'server/validations/custom.validations';

const register = {
  body: Joi.object().keys({
    password: Joi.string().required().custom(password),
    username: Joi.string().required(),
    email: Joi.string(),
    fullName: Joi.string(),
  }),
};

const login = {
  body: Joi.object().keys({
    username: Joi.string(),
    password: Joi.string(),
    fbAccessToken: Joi.string(),
    ggAccessToken: Joi.string(),
    remember: Joi.boolean(),
  }),
};

const logout = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required(),
  }),
};

const refreshTokens = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required(),
  }),
};

const forgotPassword = {
  body: Joi.object().keys({
    email: Joi.string().email().required(),
  }),
};

const resetPassword = {
  query: Joi.object().keys({
    token: Joi.string().required(),
  }),
  body: Joi.object().keys({
    password: Joi.string().required().custom(password),
  }),
};

const verifyAccount = {
  query: Joi.object().keys({
    verifyToken: Joi.string().required(),
  }),
};

export {
  register,
  login,
  logout,
  refreshTokens,
  forgotPassword,
  resetPassword,
  verifyAccount,
};
